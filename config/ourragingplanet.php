<?php

return [
    'simulation' => [
        'server' => env('SIMULATION_SERVER', '172.17.0.1'),
        'maximum_decode_memory' => env('SIMULATION_MAXIMUM_DECODE_MEMORY', '1000M')
    ],
    'legal' => [
        'terms-and-conditions-link' => 'https://s3.eu-west-2.amazonaws.com/ourragingplanet-media/subscription-terms-conditions.pdf',
        'privacy-policy-link' => 'https://s3.eu-west-2.amazonaws.com/ourragingplanet-media/privacy-and-cookies-policy.pdf'
    ]
];
