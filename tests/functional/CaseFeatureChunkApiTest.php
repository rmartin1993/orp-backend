<?php

use App\Models\CaseTier\CaseFeatureChunk;
use Symfony\Component\HttpFoundation\Response;

class CaseFeatureChunkApiTest extends FunctionalApiTest
{
    public static function getClass()
    {
        return CaseFeatureChunk::class;
    }

    public function getTransformer()
    {
        return new CaseFeatureChunkTransformer;
    }

    public function getJsonFields()
    {
        return [
            'case_context_id',
            'feature_set_id',
        ];
    }

    /**
     * @test
     */
    public function testCreateCaseFeatureChunk()
    {
        $caseFeatureChunk = $this->fake();

        $caseFeatureChunk->featureSet->save();

        $caseFeatureChunkJson = $this->reduceToJsonFields($caseFeatureChunk->toArray());

        $this->json('POST', '/api/v1/case-feature-chunks', $caseFeatureChunkJson);

        $caseFeatureChunkJson['feature_set_id'] = $caseFeatureChunk['featureSet']['slug'];

        $this->assertApiResponse($caseFeatureChunkJson);
    }

    /**
     * @test
     */
    public function testReadCaseFeatureChunk()
    {
        $caseFeatureChunk = $this->make();

        $caseFeatureChunk->feature_set_id = $caseFeatureChunk->featureSet->slug;

        $caseFeatureChunkJson = $this->reduceToJsonFields($caseFeatureChunk->toArray());

        $chunk = ['test' => 32];
        $caseFeatureChunk->chunk = json_encode($chunk);
        $caseFeatureChunk->save();
        $caseFeatureChunkJson = $chunk;

        $this->json('GET', '/api/v1/case-feature-chunks/' . $caseFeatureChunk->id);

        $this->assertApiExactResponse($chunk);
    }

    /**
     * @test
     */
    public function testUpdateCaseFeatureChunk()
    {
        $caseFeatureChunk = $this->make();

        $chunk = ['test' => 32];

        $this->json('PUT', '/api/v1/case-feature-chunks/' . $caseFeatureChunk->id, $chunk);

        $this->json('GET', '/api/v1/case-feature-chunks/' . $caseFeatureChunk->id);

        $this->assertApiExactResponse($chunk);
    }

    /**
     * @test
     */
    public function testDeleteCaseFeatureChunk()
    {
        $caseFeatureChunk = $this->make();

        $this->json('DELETE', '/api/v1/case-feature-chunks/' . $caseFeatureChunk->id);

        $this->assertResponseStatus(Response::HTTP_NO_CONTENT);

        $this->json('GET', '/api/v1/case-feature-chunks/' . $caseFeatureChunk->id);

        $this->assertResponseStatus(Response::HTTP_NOT_FOUND);
    }
}
