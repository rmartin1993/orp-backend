<?php namespace App\Helpers;

use Illuminate\Redis\Database;
use Config;
use Guzzle\Http\Exception\ServerErrorResponseException;
use League\Flysystem\FilesystemInterface;

// This class is temporary until an external job scheduler is incorporated
class OrpSimulationHelper
{
    protected $client;

    protected $expiration;

    public function __construct(\GuzzleHttp\Client $client, Database $redis, FilesystemInterface $filesystem)
    {
        $this->client = $client;
        $this->redis = $redis;
        $this->host = config('orp.host');
        $this->filesystem = $filesystem;
        $this->expiration = Config::get('ourragingplanet.simulation.expiration', 600);

        $memory = Config::get('ourragingplanet.simulation.maximum_decode_memory');
        if ($memory) {
            ini_set('memory_limit', $memory);
        }
    }

    public function persistPortfolio($id, $settings, $features)
    {
        $expiration = $this->expiration;

        $key = 'orp_simulation_' . $id . '_query';
        $this->redis->set($key, $features);
        $this->redis->expire($key, $expiration);

        $filesystem->write('simulations/' . $key . '.json', json_encode($settings));

        $query = $settings;
        $query['redis_key'] = $key;

        return $query;
    }

    public function result($id, $begin, $end, $window, $center, $featureStates, $phenomenon, $model)
    {
        $features = json_encode($featureStates);
        $settings = [
            'id' => $id,
            'version' => '2',
            'phenomenon' => $phenomenon,
            'definition' => $model,
            'model' => $model,
            'begin' => $begin,
            'end' => $end,
            'center' => $center,
            'window' => $window
        ];
        $query = $this->persistPortfolio($id, $settings, $features);

        // This approach is temporary and will be replaced with a proper job queue asap (as is used elsewhere)
        $server = Config::get('ourragingplanet.simulation.server');

        try {
            $response = $this->client->post('http://' . $server . ':5000/result/' . $id, ['query' => $settings]);
        } catch (ServerErrorResponseException $e) {
            \Log::info("Error from the simulation server...");
            \Log::info($e->getResponse()->getBody());
            return false;
        }

        $keys = json_decode($response->getBody(), true);
        $results = $this->retrieveResults($keys);

        return $results;
    }

    public function retrieveResults($keys)
    {
        $results = json_decode($this->redis->get($keys['results']), true);
        $featureArcs = json_decode($this->redis->get($keys['feature_arcs']), true);
        $this->redis->del([$keys['results'], $keys['feature_arcs']]);

        return [
            'results' => $results,
            'feature_arcs' => $featureArcs
        ];
    }
}
