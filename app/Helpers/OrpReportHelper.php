<?php namespace App\Helpers;

use Illuminate\Support\Collection;
use App\Models\CachedDataServerFeatureSet;
use PhpOffice\PhpWord\PhpWord;
use PhpOffice\PhpWord\IOFactory;
use Carbon\Carbon;
use SplTempFileObject;

class OrpReportHelper
{
    /**
     * PHPWord document generator
     *
     * @var Phpword $_phpWord
     */
    protected $phpWord;

    public function __construct()
    {
        $this->phpWord = new PhpWord;
        static::addStyles($this->phpWord);
    }

    protected static function addStyles(PhpWord $phpWord)
    {
        $phpWord->addTitleStyle(1, ['size' => 20, 'color' => '008080', 'bold' => true]);
        $phpWord->addTitleStyle(2, ['size' => 16, 'color' => '008080', 'bold' => false]);
    }

    protected static function writeToFileObject(PhpWord $phpWord)
    {
        $tmpFile = tempnam(sys_get_temp_dir(), 'orp-report');
        $writer = IOFactory::createWriter($phpWord, 'HTML');
        $writer->save($tmpFile);
        return $tmpFile;
    }

    public function exportToTemporaryFile($caseContext)
    {
        $section = $this->phpWord->addSection();
        $section->addTitle('OurRagingPlanet Exported Report', 1);
        $section->addTitle('Case: ' . $caseContext->name);
        $section->addTextBreak(2);

        $section->addText('Exported: ' . Carbon::now()->format('Y-m-d H:i:s'));
        $section->addTextBreak(2);

        $table = $section->addTable();

        $table->addRow();
        $table->addCell(3000)->addText("Owner");
        $table->addCell(5000)->addText($caseContext->owner ? $caseContext->owner->name : "(none)");

        $table->addRow();
        $table->addCell(3000)->addText("Data District");
        if ($caseContext->district) {
            $table->addCell(5000)->addText($caseContext->district->name . "(" . $caseContext->district->slug . ")");
        } else {
            $table->addCell(5000)->addText("(none)");
        }

        if ($caseContext->begins) {
            $table->addRow();
            $table->addCell(3000)->addText("Begins");
            $table->addCell(5000)->addText($caseContext->begins);
        }

        $table->addRow();
        $table->addCell(3000)->addText("Centre");
        $latLng = $caseContext->centerLatLng;
        $table->addCell(5000)->addText("(" . $latLng[0] . ", " . $latLng[1] . ")");

        $table->addRow();
        $table->addCell(3000)->addText("Extent");
        $table->addCell(5000)->addText($caseContext->extent);

        if ($caseContext->begins) {
            $table->addRow();
            $table->addCell(3000)->addText("Begins");
            $table->addCell(5000)->addText($caseContext->begins);
        }

        if ($caseContext->ends) {
            $table->addRow();
            $table->addCell(3000)->addText("Ends");
            $table->addCell(5000)->addText($caseContext->ends);
        }

        $table->addRow();
        $table->addCell(3000)->addText("");
        $table->addRow();
        $table->addCell(3000)->addText("Features");
        $caseContext->caseFeatureChunks->each(function ($featureArc) use (&$table) {
            $table->addRow();
            $table->addCell(3000)->addText($featureArc->featureSet->name);
            $table->addCell(5000)->addText($featureArc->stats);
        });

        return static::writeToFileObject($this->phpWord);
    }
}
