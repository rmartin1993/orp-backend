<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Jobs\SimulationExecute;
use App\Models\SimulationTier\Simulation;
use App\Models\Users\Affiliation;

class SimulationRun
{
    /**
     * Simulation to be run
     *
     * @var App\Models\SimulationTier\Simulation
     */
    private $simulation;

    /**
     * Simulator to be treated as running the Simulation
     *
     * @var App\Models\Users\User
     */
    private $simulator;

    /**
     * Allow overwriting of existing simulation results
     *
     * @var bool
     */
    private $overwrite = false;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($simulation, $simulator, $overwrite = false)
    {
        $this->simulation = $simulation;
        $this->simulator = $simulator;
        $this->overwrite = $overwrite;
    }

    /**
     * Execute the job.
     *
     * This gives an opportunity to any preparation required for the simulation, or to decide
     * whether it really needs to be executed in a worker, or any other events updates related.
     *
     * @return void
     */
    public function handle()
    {
        $user = $this->simulator;
        if ($user && ($this->overwrite || in_array($this->simulation->status, [Simulation::STATUS_INACTIVE, Simulation::STATUS_UNPROCESSED]))) {
            if (!$user->canSimulate()) {
                \Log::warning("Insufficient simulation credits for user " . $user->id);
                // TODO: improve notification for user
                return false;
            }

            $this->simulation->status = Simulation::STATUS_IN_PROGRESS;
            $this->simulation->updateFromCase();
            $this->simulation->save();

            dispatch(new SimulationExecute($this->simulation, $user));
        }
    }
}
