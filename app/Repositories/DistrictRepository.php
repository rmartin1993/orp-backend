<?php

namespace App\Repositories;

use App\Models\Features\District;
use InfyOm\Generator\Common\BaseRepository;

class DistrictRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'slug',
        'name'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return District::class;
    }
}
