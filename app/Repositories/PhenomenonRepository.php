<?php

namespace App\Repositories;

use App\Models\AbstractTier\Phenomenon;

class PhenomenonRepository extends SlugRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'slug',
        'name',
        'symbol',
        'color'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Phenomenon::class;
    }
}
