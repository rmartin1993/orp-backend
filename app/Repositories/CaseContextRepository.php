<?php

namespace App\Repositories;

use App\Models\CaseTier\CaseContext;
use InfyOm\Generator\Common\BaseRepository;

class CaseContextRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'district_id',
        'name',
        'center',
        'extent',
        'begins',
        'ends',
        'owner_id'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return CaseContext::class;
    }
}
