<?php

namespace App\Repositories;

use InfyOm\Generator\Common\BaseRepository;

abstract class SlugRepository extends BaseRepository
{
    /**
     * Find an item by its slug
     **/
    public function findBySlug($slug, $columns = ['*'])
    {
        return $this->findByField('slug', $slug, $columns)->first();
    }
}
