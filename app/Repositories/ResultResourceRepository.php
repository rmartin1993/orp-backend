<?php

namespace App\Repositories;

use App\Models\SimulationTier\ResultResource;
use InfyOm\Generator\Common\BaseRepository;

class ResultResourceRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'resource_id',
        'simulation_id',
        'quantity',
        'duration'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return ResultResource::class;
    }
}
