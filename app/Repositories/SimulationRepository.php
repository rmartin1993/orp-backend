<?php

namespace App\Repositories;

use App\Models\SimulationTier\Simulation;
use InfyOm\Generator\Common\BaseRepository;

class SimulationRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'case_context_id',
        'combination_id',
        'settings',
        'definition',
        'status',
        'center',
        'extent',
        'begins',
        'ends',
        'result',
        'owner_id'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Simulation::class;
    }
}
