<?php

namespace App\Repositories;

use App\Models\Interaction\WorksheetAnswer;
use InfyOm\Generator\Common\BaseRepository;

class WorksheetAnswerRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'first',
        'final',
        'mark',
        'worksheet_id',
        'challenge_id'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return WorksheetAnswer::class;
    }
}
