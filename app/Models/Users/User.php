<?php

namespace App\Models\Users;

use Eloquent as Model;

use Hash;
use App\Events\UserCreated;
use Alsofronie\Uuid\UuidModelTrait;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;
use Spatie\Permission\Traits\HasRoles;
use Laravel\Cashier\Billable;

/**
 * @SWG\Definition(
 *      definition="User",
 *      required={""},
 *      @SWG\Property(
 *          property="identifier",
 *          description="identifier",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="password",
 *          description="password",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="affiliation_id",
 *          description="affiliation_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="forename",
 *          description="forename",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="surname",
 *          description="surname",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="email",
 *          description="email",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="remember_token",
 *          description="remember_token",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class User extends Authenticatable
{

    use HasRoles;

    use HasApiTokens;

    use Notifiable;

    use UuidModelTrait;

    use Billable;

    public $table = 'users';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';



    public $fillable = [
        'identifier',
        'password',
        'affiliation_id',
        'teacher_id',
        'forename',
        'surname',
        'email',
        'remember_token'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'identifier' => 'string',
        'password' => 'string',
        'affiliation_id' => 'integer',
        'affiliation_code' => 'string',
        'forename' => 'string',
        'surname' => 'string',
        'email' => 'string',
        'remember_token' => 'string'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'identifier'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'forename' => 'required',
        'surname' => 'required',
        'district_id' => 'required|exists:districts,id',
        'affiliation_code' => 'exists:affiliations,code',
        'email' => 'required|email|unique:users',
        'password' => 'required|confirmed|min:6',
        'agreement' => 'required'
    ];

    /**
     * The event map for the model.
     *
     * @var array
     */
    protected $events = [
        'created' => UserCreated::class,
    ];

    /**
     * Check whether this user still has simulation credits, if needed
     *
     * @return boolean|integer Whether can simulate or, if metered, number of remaining simulations
     **/
    public function canSimulate()
    {
        if ($this->affiliation) {
            return $this->affiliation->canSimulate();
        }

        return false;
    }

    /**
     * Mark off a simulation credit if needs be
     *
     * @return integer|null
     **/
    public function useSimulationCredit($number)
    {
        if ($this->affiliation) {
            $remaining = $this->affiliation->useSimulationCredit($number);
            $this->affiliation->save();
            return $remaining;
        }

        return null;
    }

    /**
     * Teacher, if this user has one
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function teacher()
    {
        return $this->belongsTo(\App\Models\Users\User::class, 'teacher_id', 'id');
    }

    /**
     * Affiliation for this user
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function affiliation()
    {
        return $this->belongsTo(\App\Models\Users\Affiliation::class, 'affiliation_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function caseContexts()
    {
        return $this->hasMany(\App\Models\CaseTier\CaseContext::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function numericalModels()
    {
        return $this->hasMany(\App\Models\AbstractTier\NumericalModel::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function combinations()
    {
        return $this->hasMany(\App\Models\AbstractTier\Combination::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function simulations()
    {
        return $this->hasMany(\App\Models\SimulationTier\Simulation::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function informationals()
    {
        return $this->hasMany(\App\Models\Interaction\Informational::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function worksheets()
    {
        return $this->hasMany(\App\Models\Interaction\Worksheet::class);
    }

    /**
     * Always hash the identifier before persisting
     */
    public function setIdentifierAttribute($value)
    {
        $this->attributes['identifier'] = Hash::make($value);
    }

    /**
     * Always hash the password before persisting
     */
    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = Hash::make($value);
    }

    /**
     * Get the access token for a simulation
     *
     * @return Token
     */
    public function getSimulationToken($simulationId)
    {
        return $this->tokens()->whereName('simulation.' . $simulationId)->first();
    }

    /**
     * Get the combined name of this user
     *
     * @return string
     */
    public function getNameAttribute()
    {
        return $this->forename . " " . $this->surname;
    }
}
