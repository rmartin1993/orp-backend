<?php

namespace App\Models\CaseTier;

use Eloquent as Model;
use Carbon\Carbon;


/**
 * @SWG\Definition(
 *      definition="CaseFeatureChunk",
 *      required={""},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="feature_set_id",
 *          description="feature_set_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="stats",
 *          description="stats",
 *          type="json",
 *          format="json"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class CaseFeatureChunk extends Model
{


    public $table = 'case_feature_chunks';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    /**
     * Status values for a feature wrt a case
     *
     * @var integer
     */
    const STATUS_INACTIVE = 0;
    const STATUS_ACTIVE = 1;
    // TRANSLATE
    public static $statusLabels = [
        self::STATUS_INACTIVE => "Inactive",
        self::STATUS_ACTIVE => "Active"
    ];


    public $fillable = [
        'chunk',
        'stats',
        'status',
        'case_context_id',
        'feature_set_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'status' => 'integer',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    /**
     * FeatureSet from which this was created
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function featureSet()
    {
        return $this->belongsTo(\App\Models\Features\FeatureSet::class, 'feature_set_id', 'id');
    }

    /**
     * Case to which this applies
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function caseContext()
    {
        return $this->belongsTo(\App\Models\CaseTier\CaseContext::class, 'case_context_id', 'id');
    }

    /**
     * Simulated feature arcs
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function featureArcs()
    {
        return $this->hasMany(\App\Models\SimulationTier\FeatureArc::class);
    }

    /**
     * Filter to chunks marked for viewing
     *
     * @return \Illuminate\Database\Query\Builder
     **/
    public function scopeActive($query)
    {
        return $query->whereStatus(self::STATUS_ACTIVE);
    }
}
