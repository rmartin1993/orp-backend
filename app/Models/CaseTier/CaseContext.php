<?php

namespace App\Models\CaseTier;

use Eloquent as Model;

use Carbon\Carbon;
use Phaza\LaravelPostgis\Geometries\Point;
use Alsofronie\Uuid\UuidModelTrait;
use Phaza\LaravelPostgis\Eloquent\PostgisTrait;
use Auth;

class CaseContext extends Model
{

    use PostgisTrait;

    use UuidModelTrait;

    public $table = 'case_contexts';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    public $fillable = [
        'district_id',
        'name',
        'center',
        'extent',
        'begins',
        'ends',
        'owner_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'district_id' => 'integer',
        'name' => 'string',
        'extent' => 'string'
    ];

    /**
     * The attributes that are dates.
     *
     * @var array
     */
    protected $dates = [
        'begins',
        'ends',
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    /**
     * Fields that contain GIS data
     *
     * @var array
     */
    public $postgisFields = [
        'center',
        'extent'
    ];

    /**
     * Owner of this object
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function owner()
    {
        return $this->belongsTo(\App\Models\Users\User::class, 'owner_id', 'id');
    }

    /**
     * District within which this case fits
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function district()
    {
        return $this->belongsTo(\App\Models\Features\District::class, 'district_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function simulations()
    {
        return $this->hasMany(\App\Models\SimulationTier\Simulation::class);
    }

    /**
     * Which chunks have been created from this for the case?
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function caseFeatureChunks()
    {
        return $this->hasMany(\App\Models\CaseTier\CaseFeatureChunk::class);
    }

    /**
     * Which chunks are marked to be visible?
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function activeFeatureChunks()
    {
        return $this->caseFeatureChunks()->active();
    }

    /**
     * Which feature sets appear in chunks in the case?
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasManyThrough
     **/
    public function featureSets()
    {
        return $this->belongsToMany(
            \App\Models\Features\FeatureSet::class,
            'case_feature_chunks'
        );
    }

    /**
     * This allows the centre to be set from a latitude/longitude pair
     **/
    public function setCenterLatLngAttribute($center)
    {
        $this->setAttribute('center', new Point($center[0], $center[1]));
    }

    /**
     * This retrieves the centre as a latitude/longitude pair
     *
     * @return array|null
     **/
    public function getCenterLatLngAttribute()
    {
        $point = $this->attributes['center'];
        if ($point) {
            return [$point->getLat(), $point->getLng()];
        }
        return null;
    }

    /**
     * Get a count for display in the dashboard or similar
     *
     * @return integer
     **/
    public function getDashboardCountAttribute()
    {
        $user = Auth::user();
        if (!$user) {
            return 0;
        } elseif ($user->hasRole('admin')) {
            return $this->count();
        }

        return $this->whereOwnerId($user->id)->count();
    }
}
