<?php

namespace App\Models\SimulationTier;

use Eloquent as Model;

use Auth;
use Carbon\Carbon;
use App\Events\SimulationCreated;
use App\Models\Users\User;
use Phaza\LaravelPostgis\Geometries\Point;
use Alsofronie\Uuid\UuidModelTrait;
use Phaza\LaravelPostgis\Eloquent\PostgisTrait;

class Simulation extends Model
{

    use UuidModelTrait;

    use PostgisTrait;

    public $table = 'simulations';

    /**
     * Status values for a simulation
     *
     * @var integer
     */
    const STATUS_INACTIVE = 0;
    const STATUS_UNPROCESSED = 1;
    const STATUS_IN_PROGRESS = 2;
    const STATUS_COMPLETE = 3;
    const STATUS_FAILED = 4;
    // TRANSLATE
    public static $statusLabels = [
        self::STATUS_INACTIVE => "Inactive",
        self::STATUS_UNPROCESSED => "Unprocessed",
        self::STATUS_IN_PROGRESS => "In-progress",
        self::STATUS_COMPLETE => "Complete",
        self::STATUS_FAILED => "Failed"
    ];

    const SHARED_PRIVATE = 0;
    const SHARED_GLOBAL = 1;
    // TRANSLATE
    public static $sharedLabels = [
        self::SHARED_PRIVATE => "Private",
        self::SHARED_GLOBAL => "Available to all",
    ];

    /**
     * The attributes that are dates.
     *
     * @var array
     */
    protected $dates = [
        'begins',
        'ends',
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    public $fillable = [
        'case_context_id',
        'combination_id',
        'settings',
        'definition',
        'status',
        'shared',
        'center',
        'extent',
        'begins',
        'ends',
        'result',
        'owner_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'definition' => 'string',
        'status' => 'integer',
        'shared' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    /**
     * Fields that contain GIS data
     *
     * @var array
     */
    public $postgisFields = [
        'center',
        'extent'
    ];

    /**
     * The event map for the model.
     *
     * @var array
     */
    protected $events = [
        'created' => SimulationCreated::class,
    ];

    /**
     * Owner of this object
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function owner()
    {
        return $this->belongsTo(\App\Models\Users\User::class, 'owner_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function combination()
    {
        return $this->belongsTo(\App\Models\AbstractTier\Combination::class, 'combination_id', 'id');
    }

    /**
     * Case that this simulates
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function caseContext()
    {
        return $this->belongsTo(\App\Models\CaseTier\CaseContext::class, 'case_context_id', 'id');
    }

    /**
     * Resources that this simulation shows are required
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function resultResources()
    {
        return $this->hasMany(\App\Models\SimulationTier\ResultResource::class);
    }

    /**
     * Arcs calculated by this simulation
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function featureArcs()
    {
        return $this->hasMany(\App\Models\SimulationTier\FeatureArc::class);
    }

    /**
     * Analytics that this simulation provides
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function resultAnalytics()
    {
        return $this->hasMany(\App\Models\SimulationTier\ResultAnalytic::class);
    }

    /**
     * Get challenges set for students in this case
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function challenges()
    {
        return $this->hasMany(\App\Models\Interaction\Challenge::class);
    }

    /**
     * Get informational displays for this case
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function informationals()
    {
        return $this->hasMany(\App\Models\Interaction\Informational::class);
    }

    /**
     * Get student worksheets for this case
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function worksheets()
    {
        return $this->hasMany(\App\Models\Interaction\Worksheet::class);
    }

    /**
     * Feature chunks that this simulation adds
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function resultFeatureChunks()
    {
        return $this->hasMany(\App\Models\SimulationTier\ResultFeatureChunk::class);
    }

    public function setCaseContextIdAttribute($caseContextId)
    {
        $this->attributes['case_context_id'] = $caseContextId;
        $this->updateFromCase();
    }

    public function getGeneratedTitleAttribute()
    {
        $name = "";
        if (isset($this->combination->numericalModel->phenomenon)) {
            $name .= $this->combination->numericalModel->phenomenon->name . ', ';
        }

        if (isset($this->caseContext)) {
            $name .= $this->caseContext->name;
        }

        return $name ?: "(untitled)";
    }

    /**
     * Get a count for display in the dashboard or similar
     *
     * @return integer
     **/
    public function getDashboardCountAttribute()
    {
        $user = Auth::user();
        if (!$user) {
            return 0;
        } elseif ($user->hasRole('admin')) {
            return $this->count();
        }

        return $this->whereOwnerId($user->id)->count();
    }

    /**
     * Create a new Simulation from an existing one and attach it
     * to the current user as an owner
     *
     * @return \App\Models\SimulationTier\Simulation
     */
    public static function takeACopy(User $user, Simulation $from)
    {
        if (!$user) {
            throw new Exception('Cannot copy a simulation without a logged-in user');
        }

        $from->relations = [];
        $from->load('informationals', 'caseContext', 'challenges');

        $caseContext = $from->caseContext->replicate();
        $caseContext->owner_id = $user->id;
        $caseContext->push();

        $simulation = $from->replicate();
        $simulation->shared = self::SHARED_PRIVATE;
        $simulation->owner_id = $user->id;
        $simulation->case_context_id = $caseContext->id;
        $simulation->push();

        $from->informationals->each(function ($informational) use ($simulation) {
            $newInformational = $informational->replicate();
            $newInformational->simulation_id = $simulation->id;
            $newInformational->push();
        });

        $from->challenges->each(function ($challenge) use ($simulation) {
            $newChallenge = $challenge->replicate();
            $newChallenge->simulation_id = $simulation->id;
            $newChallenge->push();
        });

        return $simulation;
    }

    /**
     * Update the properties of this simulation from its CaseContext
     *
     * @return boolean $success
     */
    public function updateFromCase()
    {
        if ($this->caseContext) {
            $this->begins = $this->caseContext->begins;
            $this->ends = $this->caseContext->ends;
            $this->center = $this->caseContext->center;
            $this->extent = $this->caseContext->extent;
            return true;
        }

        return false;
    }
}
