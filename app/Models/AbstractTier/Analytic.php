<?php

namespace App\Models\AbstractTier;

use Eloquent as Model;


/**
 * @SWG\Definition(
 *      definition="Analytic",
 *      required={""},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="code",
 *          description="code",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="name",
 *          description="name",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class Analytic extends Model
{


    public $table = 'analytics';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';



    public $fillable = [
        'code',
        'name'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'code' => 'string',
        'name' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function challengeTemplates()
    {
        return $this->hasMany(\App\Models\Interaction\ChallengeTemplate::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     **/
    public function analytics()
    {
        return $this->belongsToMany(\App\Models\AbstractTier\Analytic::class, 'result_analytics');
    }

    /**
     * Numerical models that can supply this analytic
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     **/
    public function numericalModels()
    {
        return $this->belongsToMany(\App\Models\AbstractTier\NumericalModel::class, 'analytic_numerical_model');
    }
}
