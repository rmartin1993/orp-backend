<?php

namespace App\Models\Features;

use Eloquent as Model;

use Cviebrock\EloquentSluggable\Sluggable;

class FeatureSet extends Model
{
    use Sluggable;


    public $table = 'feature_sets';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';



    public $fillable = [
        'slug',
        'name',
        'description'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'slug' => 'string',
        'name' => 'string',
        'description' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable() {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }

    /**
     * Statuses indicating availability in a district
     */
    const DISTRICT_INACTIVE = 0;
    const DISTRICT_ACTIVE = 1;
    const DISTRICT_SUSPENDED = 2;
    const DISTRICT_EXPIRED = 3;

    /**
     * Case-based feature chunks
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function caseFeatureChunks()
    {
        return $this->hasMany(\App\Models\CaseTier\CaseFeatureChunk::class);
    }

    /**
     * Simulation-based feature chunks
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function resultFeatureChunks()
    {
        return $this->hasMany(\App\Models\SimulationTier\ResultFeatureChunk::class);
    }

    /**
     * Districts within which this feature set is available
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     **/
    public function districts()
    {
        return $this->belongsToMany(\App\Models\Features\District::class, 'district_feature_set')
            ->withPivot('status', 'data_server_set_id', 'data_server')
            ->withTimestamps();
    }
}
