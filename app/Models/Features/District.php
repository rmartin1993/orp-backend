<?php

namespace App\Models\Features;

use Eloquent as Model;

use Carbon\Carbon;
use Cviebrock\EloquentSluggable\Sluggable;

class District extends Model
{
    use Sluggable;


    public $table = 'districts';

    public $fillable = [
        'slug',
        'name'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'slug' => 'string',
        'name' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable() {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }

    /**
     * Affiliations located within (or otherwise exclusively tied to) this district
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function affiliations()
    {
        return $this->hasMany(\App\Models\Users\Affiliation::class);
    }

    /**
     * Cases that are set-up within this district
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function caseContexts()
    {
        return $this->hasMany(\App\Models\CaseTier\CaseContext::class);
    }

    /**
     * FeatureSets that are available within this district
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     **/
    public function featureSets()
    {
        return $this->belongsToMany(\App\Models\Features\FeatureSet::class, 'district_feature_set')
            ->withPivot('status', 'data_server_set_id', 'data_server')
            ->withTimestamps();
    }
}
