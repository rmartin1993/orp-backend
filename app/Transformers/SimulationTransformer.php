<?php

namespace App\Transformers;

use App\Models\SimulationTier\Simulation;
use League\Fractal;

/**
 * @SWG\Definition(
 *      definition="Simulation",
 *      required={""},
 *      @SWG\Property(
 *          property="status",
 *          description="status",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="center",
 *          description="center",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="extent",
 *          description="extent",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="begins",
 *          description="begins",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="ends",
 *          description="ends",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="owner_id",
 *          description="Person with primary responsibility for this simulation",
 *          type="string",
 *          format="uuid"
 *      ),
 *      @SWG\Property(
 *          property="case_context_id",
 *          description="case_context",
 *          type="string",
 *          format="uuid"
 *      ),
 *      @SWG\Property(
 *          property="phenomenon_id",
 *          description="phenomenon slug",
 *          type="string",
 *          format="slug"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class SimulationTransformer extends Fractal\TransformerAbstract
{
    protected $defaultIncludes = [
            'featureArcs',
    ];

    public function transform(Simulation $simulation)
    {
        $phenomenonSlug = null;
        if ($simulation->combination_id) {
            $combination = $simulation->combination;
            if ($combination->numerical_model_id) {
                $numericalModel = $combination->numericalModel;
                if ($numericalModel->phenomenon_id) {
                    $phenomenon = $numericalModel->phenomenon;
                    $phenomenonSlug = $phenomenon->slug;
                }
            }
        }

        $simulationArray = $simulation->toArray();

        return [
            'id' => $simulationArray['id'],
            'status' => $simulationArray['status'],
            'settings' => json_decode($simulationArray['settings'], true),
            'center' => $simulationArray['center'],
            'extent' => $simulationArray['extent'],
            'begins' => $simulationArray['begins'],
            'ends' => $simulationArray['ends'],
            'owner_id' => $simulationArray['owner_id'],
            'result' => json_decode($simulationArray['result'], true),
            'phenomenon_id' => $phenomenonSlug,
            'case_context_id' => $simulationArray['case_context_id'],
            'created_at' => $simulationArray['created_at'],
            'updated_at' => $simulationArray['updated_at']
        ];
    }

    public function includeFeatureArcs(Simulation $simulation)
    {
        $featureArcs = $simulation->featureArcs;

        return $this->collection($featureArcs, new FeatureArcTransformer);
    }
}
