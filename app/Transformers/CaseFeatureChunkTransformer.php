<?php

namespace App\Transformers;

use App\Models\CaseTier\CaseFeatureChunk;
use League\Fractal;

/**
 * @SWG\Definition(
 *      definition="CaseFeatureChunk",
 *      required={""},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="feature_set_id",
 *          description="feature_set_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="case_context_id",
 *          description="Case ID",
 *          type="string",
 *          format="uuid"
 *      ),
 *      @SWG\Property(
 *          property="chunk",
 *          description="array of GeoJSON objects representing this chunk",
 *          type="array",
 *          format="array"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class CaseFeatureChunkTransformer extends Fractal\TransformerAbstract
{
    public function transform(CaseFeatureChunk $caseFeatureChunk)
    {
        $caseFeatureChunkArray = $caseFeatureChunk->toArray();

        $featureSetSlug = $caseFeatureChunk->featureSet ? $caseFeatureChunk->featureSet->slug : null;

        return [
            'id' => $caseFeatureChunkArray['id'],
            'case_context_id' => $caseFeatureChunkArray['case_context_id'],
            'feature_set_id' => $featureSetSlug,
            'created_at' => $caseFeatureChunkArray['created_at'],
            'updated_at' => $caseFeatureChunkArray['updated_at']
        ];
    }
}
