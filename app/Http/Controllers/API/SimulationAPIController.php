<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateSimulationAPIRequest;
use App\Http\Requests\API\UpdateSimulationAPIRequest;
use App\Models\SimulationTier\Simulation;
use App\Repositories\SimulationRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Transformers\SimulationTransformer;
use App\Transformers\ResultFeatureChunkTransformer;
use App\Transformers\InformationalTransformer;
use App\Transformers\ChallengeTransformer;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Symfony\Component\HttpFoundation\Response;
use DB;
use Auth;
use Spatie\Fractal\Fractal;
use App\Jobs\SimulationRun;

/**
 * Class SimulationController
 * @package App\Http\Controllers\API
 */

class SimulationAPIController extends AppBaseController
{
    /** @var  SimulationRepository */
    private $simulationRepository;

    public function __construct(SimulationRepository $simulationRepo, Auth $auth)
    {
        $this->simulationRepository = $simulationRepo;
        $this->auth = $auth;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/simulations",
     *      summary="Get a listing of the Simulations.",
     *      tags={"Simulation"},
     *      description="Get all Simulations",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Simulation")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */

    public function index(Request $request)
    {
        $this->simulationRepository->pushCriteria(new RequestCriteria($request));
        $this->simulationRepository->pushCriteria(new LimitOffsetCriteria($request));
        $simulations = $this->simulationRepository->all();

        return fractal($simulations, new SimulationTransformer)->respond();
    }

    /**
     * @param CreateSimulationAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/simulations",
     *      summary="Store a newly created Simulation in storage",
     *      tags={"Simulation"},
     *      description="Store Simulation",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Simulation that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Simulation")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Simulation"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */

    public function store(CreateSimulationAPIRequest $request)
    {
        $input = $request->all();

        if (array_key_exists('settings', $input)) {
            $input['settings'] = json_encode($input['settings']);
        }

        if (array_key_exists('result', $input)) {
            $input['result'] = json_encode($input['result']);
        }

        $simulation = $this->simulationRepository->create($input);

        return fractal($simulation, new SimulationTransformer)->respond();
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/simulations/{id}",
     *      summary="Display the specified Simulation",
     *      tags={"Simulation"},
     *      description="Get Simulation",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Simulation",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Simulation"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */

    public function show($id)
    {
        /** @var Simulation $simulation */
        $simulation = $this->simulationRepository->findWithoutFail($id);

        if (empty($simulation)) {
            return $this->sendError('Simulation not found');
        }

        return fractal($simulation, new SimulationTransformer)->respond();
    }

    /**
     * @param int $id
     * @param UpdateSimulationAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/simulations/{id}",
     *      summary="Update the specified Simulation in storage",
     *      tags={"Simulation"},
     *      description="Update Simulation",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Simulation",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Simulation that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Simulation")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Simulation"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */

    public function update($id, UpdateSimulationAPIRequest $request)
    {
        $input = $request->all();

        if (array_key_exists('settings', $input)) {
            $input['settings'] = json_encode($input['settings']);
        }

        if (array_key_exists('result', $input)) {
            $input['result'] = json_encode($input['result']);
        }

        /** @var Simulation $simulation */
        $simulation = $this->simulationRepository->findWithoutFail($id);

        if (empty($simulation)) {
            return $this->sendError('Simulation not found');
        }

        $simulation = $this->simulationRepository->update($input, $id);

        return fractal($simulation, new SimulationTransformer)->respond();
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/simulations/{id}",
     *      summary="Remove the specified Simulation from storage",
     *      tags={"Simulation"},
     *      description="Delete Simulation",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Simulation",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */

    public function destroy($id)
    {
        /** @var Simulation $simulation */
        $simulation = $this->simulationRepository->findWithoutFail($id);

        if (empty($simulation)) {
            return $this->sendError('Simulation not found');
        }

        $this->simulationRepository->delete($simulation->id);

        return response(null, Response::HTTP_NO_CONTENT);
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/simulations/{id}/feature-arcs/{featureArcId}",
     *      summary="Display the specified Simulation",
     *      tags={"Simulation"},
     *      description="Get Simulation Feature arc",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Simulation",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="featureArcId",
     *          description="id of Feature Arc",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="json"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function featureArcShow($id, $featureArcId)
    {
        /** @var Simulation $simulation */
        $simulation = $this->simulationRepository->with(['featureArcs'])->findWithoutFail($id);

        if (empty($simulation)) {
            return $this->sendError('Simulation not found');
        }

        $featureArc = $simulation
            ->featureArcs()
            ->where('feature_arcs.id', '=', $featureArcId)
            ->first();

        if (empty($featureArc)) {
            return $this->sendError('Feature arc not found');
        }

        return response($featureArc->arc, 200)
            ->header('Content-Type', 'application/json');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/simulations/{id}/features/{featureChunkId}",
     *      summary="Display the specified ResultFeatureChunk JSON",
     *      tags={"Simulation"},
     *      description="Get Simulation Feature chunk",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Simulation",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="featureChunkId",
     *          description="id of ResultFeatureChunk",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="json"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function featuresShow($id, $featureChunkId)
    {
        /** @var Simulation $simulation */
        $simulation = $this->simulationRepository->with(['resultFeatureChunks'])->findWithoutFail($id);

        if (empty($simulation)) {
            return $this->sendError('Simulation not found');
        }

        $resultFeatureChunk = $simulation
            ->resultFeatureChunks()
            ->where('result_feature_chunks.id', '=', $featureChunkId)
            ->first();

        return response($resultFeatureChunk->arc, 200)
            ->header('Content-Type', 'application/json');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/simulations/{id}/features",
     *      summary="Display the calculated features of the specified Simulation",
     *      tags={"Simulation"},
     *      description="Get Simulation Features",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Simulation",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="array",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/ResultFeatureChunk"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */

    public function featuresIndex(Request $request, $id)
    {
        /** @var Simulation $simulation */
        $simulation = $this->simulationRepository->with(['resultFeatureChunks'])->findWithoutFail($id);

        if (empty($simulation)) {
            return $this->sendError('Simulation not found');
        }

        $resultFeatureChunks = $simulation->resultFeatureChunks();

        if ($request->has('time_offset')) {
            // Note that the (int) is _required_ to sanitize
            $integerTimeOffset = (int)$request->get('time_offset');
            $resultFeatureChunks = $resultFeatureChunks->orderBy(DB::raw('ABS(time_offset - ' . $integerTimeOffset . ')'));
        }

        $resultFeatureChunks = $resultFeatureChunks
            ->limit(2)
            ->get()
            ->sortBy('time_offset');

        return fractal($resultFeatureChunks, new ResultFeatureChunkTransformer)->respond();
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Post(
     *      path="/simulations/{id}/run",
     *      summary="Run the specified Simulation",
     *      tags={"Simulation"},
     *      description="Run Simulation",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Simulation",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="force",
     *          description="overwrite existing results",
     *          type="boolean",
     *          required=false
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Simulation"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function run(Request $request, $id)
    {
        /** @var Simulation $simulation */
        $simulation = $this->simulationRepository->with(['resultFeatureChunks'])->findWithoutFail($id);

        $user = $this->auth->user();

        if (empty($simulation)) {
            return $this->sendError('Simulation not found');
        }

        $force = $request->has('force') && $request->get('force');
        if ($force) {
            $simulation->status = Simulation::STATUS_UNPROCESSED;
            $simulation->save();
        }

        dispatch(new SimulationRun($simulation, $user));

        return fractal($simulation, new SimulationTransformer)->respond();
    }

    /** Informationals **/

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/simulations/{id}/informationals",
     *      summary="Get a listing of the Informationals for a Simulation.",
     *      tags={"Simulation", "Informational"},
     *      description="Get all Informationals for a Simulation",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Informational",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Informational")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */

    public function informationalIndex(Request $request, $id)
    {
        /** @var Simulation $simulation */
        $simulation = $this->simulationRepository->findWithoutFail($id);

        if (empty($simulation)) {
            return $this->sendError('Simulation not found');
        }

        $informationals = $simulation->informationals->sortBy('time_offset');

        return fractal($informationals, new InformationalTransformer)->respond();
    }

    /** Challenges **/

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/simulations/{id}/challenges",
     *      summary="Get a listing of the Challenges for a Simulation.",
     *      tags={"Simulation", "Challenge"},
     *      description="Get all Challenges for a Simulation",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Challenge",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Challenge")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */

    public function challengeIndex(Request $request, $id)
    {
        /** @var Simulation $simulation */
        $simulation = $this->simulationRepository->findWithoutFail($id);

        if (empty($simulation)) {
            return $this->sendError('Simulation not found');
        }

        $challenges = $simulation->challenges->sortBy('time_offset');

        return fractal($challenges, new ChallengeTransformer)->respond();
    }
}
