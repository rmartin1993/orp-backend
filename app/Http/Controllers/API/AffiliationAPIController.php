<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateAffiliationAPIRequest;
use App\Http\Requests\API\UpdateAffiliationAPIRequest;
use App\Models\Users\Affiliation;
use App\Repositories\AffiliationRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Transformers\AffiliationTransformer;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Symfony\Component\HttpFoundation\Response;
use Spatie\Fractal\Fractal;

/**
 * Class AffiliationController
 * @package App\Http\Controllers\API
 */

class AffiliationAPIController extends AppBaseController
{
    /** @var  AffiliationRepository */
    private $affiliationRepository;

    public function __construct(AffiliationRepository $affiliationRepo)
    {
        $this->affiliationRepository = $affiliationRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/affiliations",
     *      summary="Get a listing of the Affiliations.",
     *      tags={"Affiliation"},
     *      description="Get all Affiliations",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Affiliation")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */

    public function index(Request $request)
    {
        $this->affiliationRepository->pushCriteria(new RequestCriteria($request));
        $this->affiliationRepository->pushCriteria(new LimitOffsetCriteria($request));
        $affiliations = $this->affiliationRepository->all();

        return $this->sendResponse($affiliations->toArray(), 'Affiliations retrieved successfully');
    }

    /**
     * @param CreateAffiliationAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/affiliations",
     *      summary="Store a newly created Affiliation in storage",
     *      tags={"Affiliation"},
     *      description="Store Affiliation",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Affiliation that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Affiliation")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Affiliation"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */

    public function store(CreateAffiliationAPIRequest $request)
    {
        $input = $request->all();

        $affiliation = $this->affiliationRepository->create($input);

        return fractal($affiliation, new AffiliationTransformer)->respond();
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/affiliations/{id}",
     *      summary="Display the specified Affiliation",
     *      tags={"Affiliation"},
     *      description="Get Affiliation",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Affiliation",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Affiliation"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */

    public function show($id)
    {
        /** @var Affiliation $affiliation */
        $affiliation = $this->affiliationRepository->findWithoutFail($id);

        if (empty($affiliation)) {
            return $this->sendError('Affiliation not found');
        }

        return fractal($affiliation, new AffiliationTransformer)->respond();
    }

    /**
     * @param int $id
     * @param UpdateAffiliationAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/affiliations/{id}",
     *      summary="Update the specified Affiliation in storage",
     *      tags={"Affiliation"},
     *      description="Update Affiliation",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Affiliation",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Affiliation that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Affiliation")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Affiliation"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */

    public function update($id, UpdateAffiliationAPIRequest $request)
    {
        $input = $request->all();

        /** @var Affiliation $affiliation */
        $affiliation = $this->affiliationRepository->findWithoutFail($id);

        if (empty($affiliation)) {
            return $this->sendError('Affiliation not found');
        }

        $affiliation = $this->affiliationRepository->update($input, $id);

        return fractal($affiliation, new AffiliationTransformer)->respond();
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/affiliations/{id}",
     *      summary="Remove the specified Affiliation from storage",
     *      tags={"Affiliation"},
     *      description="Delete Affiliation",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Affiliation",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */

    public function destroy($id)
    {
        /** @var Affiliation $affiliation */
        $affiliation = $this->affiliationRepository->findWithoutFail($id);

        if (empty($affiliation)) {
            return $this->sendError('Affiliation not found');
        }

        $affiliation->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
