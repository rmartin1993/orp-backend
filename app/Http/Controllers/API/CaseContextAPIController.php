<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateCaseContextAPIRequest;
use App\Http\Requests\API\UpdateCaseContextAPIRequest;
use App\Models\CaseTier\CaseContext;
use App\Transformers\CaseContextTransformer;
use App\Transformers\InformationalTransformer;
use App\Models\Interaction\Informational;
use App\Repositories\CaseContextRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Symfony\Component\HttpFoundation\Response;
use Spatie\Fractal\Fractal;

/**
 * Class CaseContextController
 * @package App\Http\Controllers\API
 */

class CaseContextAPIController extends AppBaseController
{
    /** @var  CaseContextRepository */
    private $caseContextRepository;

    public function __construct(CaseContextRepository $caseContextRepo, Fractal $fractal)
    {
        $this->caseContextRepository = $caseContextRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/case-contexts",
     *      summary="Get a listing of the CaseContexts.",
     *      tags={"CaseContext"},
     *      description="Get all CaseContexts",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/CaseContext")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */

    public function index(Request $request)
    {
        $this->caseContextRepository->pushCriteria(new RequestCriteria($request));
        $this->caseContextRepository->pushCriteria(new LimitOffsetCriteria($request));
        $caseContexts = $this->caseContextRepository->all();

        return fractal($caseContexts, new CaseContextTransformer)->respond();
    }

    /**
     * @param CreateCaseContextAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/case-contexts",
     *      summary="Store a newly created CaseContext in storage",
     *      tags={"CaseContext"},
     *      description="Store CaseContext",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="CaseContext that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/CaseContext")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/CaseContext"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */

    public function store(CreateCaseContextAPIRequest $request)
    {
        $input = $request->all();

        $caseContext = $this->caseContextRepository->create($input);

        return fractal($caseContext, new CaseContextTransformer)->respond();
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/case-contexts/{id}",
     *      summary="Display the specified CaseContext",
     *      tags={"CaseContext"},
     *      description="Get CaseContext",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of CaseContext",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/CaseContext"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */

    public function show($id)
    {
        /** @var CaseContext $caseContext */
        $caseContext = $this->caseContextRepository->findWithoutFail($id);

        if (empty($caseContext)) {
            return $this->sendError('Case Context not found');
        }

        return fractal($caseContext, new CaseContextTransformer)
            ->includeFeatureSets()
            ->respond();
    }

    /**
     * @param int $id
     * @param UpdateCaseContextAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/case-contexts/{id}",
     *      summary="Update the specified CaseContext in storage",
     *      tags={"CaseContext"},
     *      description="Update CaseContext",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of CaseContext",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="CaseContext that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/CaseContext")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/CaseContext"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */

    public function update($id, UpdateCaseContextAPIRequest $request)
    {
        $input = $request->all();

        /** @var CaseContext $caseContext */
        $caseContext = $this->caseContextRepository->findWithoutFail($id);

        if (empty($caseContext)) {
            return $this->sendError('Case Context not found');
        }

        $caseContext = $this->caseContextRepository->update($input, $id);

        return fractal($caseContext, new CaseContextTransformer)
            ->includeFeatureSets()
            ->respond();
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/case-contexts/{id}",
     *      summary="Remove the specified CaseContext from storage",
     *      tags={"CaseContext"},
     *      description="Delete CaseContext",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of CaseContext",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */

    public function destroy($id)
    {
        /** @var CaseContext $caseContext */
        $caseContext = $this->caseContextRepository->findWithoutFail($id);

        if (empty($caseContext)) {
            return $this->sendError('Case Context not found');
        }

        $this->caseContextRepository->delete($id);

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
