<?php

namespace App\Http\Controllers\API;

use App\Transformers\CaseFeatureChunkTransformer;
use App\Http\Requests\API\CreateCaseFeatureChunkAPIRequest;
use App\Http\Requests\API\UpdateCaseFeatureChunkAPIRequest;
use App\Models\CaseTier\CaseFeatureChunk;
use App\Repositories\CaseFeatureChunkRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class CaseFeatureChunkController
 * @package App\Http\Controllers\API
 */

class CaseFeatureChunkAPIController extends AppBaseController
{
    /** @var  CaseFeatureChunkRepository */
    private $caseFeatureChunkRepository;

    public function __construct(CaseFeatureChunkRepository $caseFeatureChunkRepo)
    {
        $this->caseFeatureChunkRepository = $caseFeatureChunkRepo;
    }

    /**
     * @param CreateCaseFeatureChunkAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/case-feature-chunks",
     *      summary="Store a newly created CaseFeatureChunk in storage",
     *      tags={"CaseFeatureChunk"},
     *      description="Store CaseFeatureChunk",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="CaseFeatureChunk that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/CaseFeatureChunk")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/CaseFeatureChunk"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */

    public function store(CreateCaseFeatureChunkAPIRequest $request)
    {
        $input = $request->all();

        $caseFeatureChunk = $this->caseFeatureChunkRepository->create($input);

        return fractal($caseFeatureChunk, new CaseFeatureChunkTransformer)->respond();
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/case-feature-chunks/{id}",
     *      summary="Display the specified CaseFeatureChunk",
     *      tags={"CaseFeatureChunk"},
     *      description="Get CaseFeatureChunk",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of CaseFeatureChunk",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      )
     * )
     */

    public function show($id)
    {
        /** @var CaseFeatureChunk $caseFeatureChunk */
        $caseFeatureChunk = $this->caseFeatureChunkRepository->findWithoutFail($id);

        if (empty($caseFeatureChunk)) {
            return $this->sendError('Case Feature Chunk not found');
        }

        return $caseFeatureChunk->chunk;
    }

    /**
     * @param int $id
     * @param UpdateCaseFeatureChunkAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/case-feature-chunks/{id}",
     *      summary="Update the specified CaseFeatureChunk in storage",
     *      tags={"CaseFeatureChunk"},
     *      description="Update CaseFeatureChunk",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of CaseFeatureChunk",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="CaseFeatureChunk that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/CaseFeatureChunk")
     *      )
     * )
     */

    public function update($id, UpdateCaseFeatureChunkAPIRequest $request)
    {
        /** @var CaseFeatureChunk $caseFeatureChunk */
        $caseFeatureChunk = $this->caseFeatureChunkRepository->findWithoutFail($id);

        if (empty($caseFeatureChunk)) {
            return $this->sendError('Case Feature Chunk not found');
        }

        $caseFeatureChunk = $this->caseFeatureChunkRepository->update([
            'chunk' => $request->getContent()
        ], $id);

        return fractal($caseFeatureChunk, new CaseFeatureChunkTransformer)
            ->respond();
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/case-feature-chunks/{id}",
     *      summary="Remove the specified CaseFeatureChunk from storage",
     *      tags={"CaseFeatureChunk"},
     *      description="Delete CaseFeatureChunk",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of CaseFeatureChunk",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */

    public function destroy($id)
    {
        /** @var CaseFeatureChunk $caseFeatureChunk */
        $caseFeatureChunk = $this->caseFeatureChunkRepository->findWithoutFail($id);

        if (empty($caseFeatureChunk)) {
            return $this->sendError('Case Feature Chunk not found');
        }

        $this->caseFeatureChunkRepository->delete($id);

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
