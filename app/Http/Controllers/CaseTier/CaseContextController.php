<?php 

namespace App\Http\Controllers\CaseTier;

use Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use \Serverfireteam\Panel\CrudController;
use App\Models\Features\FeatureSet;
use App\Models\CaseTier\CaseFeatureChunk;
use App\Models\CaseTier\CaseContext;
use App\Helpers\OrpReportHelper;

use Illuminate\Http\Request;
use Response;

class CaseContextController extends CrudController
{

    public function __construct(\Lang $lang, Request $request)
    {
        parent::__construct($lang);

        $this->request = $request;
        $this->features['all']['import-button'] = false;
        $this->features['all']['export-button'] = false;
    }

    public function all($entity)
    {
        parent::all($entity);

        $caseContextModel = new \App\Models\CaseTier\CaseContext();
        if (!Auth::user()->hasRole('admin')) {
            $caseContextModel = $caseContextModel->whereOwnerId(Auth::user()->id);
        }
        $this->filter = \DataFilter::source($caseContextModel);

        $this->filter->add('name', 'Name', 'text');
        $this->filter->submit('search');
        $this->filter->reset('reset');
        $this->filter->build();

        $this->grid = \DataGrid::source($this->filter);
        $modifyTarget = url('panel/' . $entity . '/edit?modify={{ $id }}');
        $this->grid->add('name', 'Name')->link($modifyTarget);
        $this->grid->add('created_at', 'Created');
        $this->grid->add('updated_at', 'Updated');

        $addUrl = url('panel', $entity . '/edit') . '?step=1';
        $this->grid->add_url = $addUrl;

        $viewTarget = url('panel/SimulationTier:Simulation/edit?case_context_id={{ $id }}');
        $this->grid->add('{{ "Add Simulation" }}', 'Add Simulation')->link($viewTarget);

        $viewTarget = url('panel/SimulationTier:Simulation/all?search=1&case_context_id={{ $id }}');
        $this->grid->add('{{ "Show Simulations" }}', 'Show Simulations')->link($viewTarget);

        $downloadTarget = url('panel/CaseTier:CaseContext/downloadReport/{{ $id }}');
        $this->grid->add('{{ "Report [as HTML]" }}', 'Download Report')->link($downloadTarget);

        $this->addStylesToGrid();

        $this->grid->orderBy('updated_at', 'desc');

        return $this->returnView('panelViews::all', ['title' => 'Target Zones']);
    }

    public function edit($entity)
    {
        parent::edit($entity);

        $user = Auth::user();

        $this->edit = \DataEdit::source(new \App\Models\CaseTier\CaseContext());

        if (!$this->edit->model->id && !$this->request->get('step')) {
            return redirect(url('panel', $entity . '/edit') . '?step=1');
        }

        $this->edit->label('Edit Target Zone');

        $this->edit->add('name', 'Name', 'text')->rule('required');

        if ($this->edit->model->id) {
            $featureOptions = [];
            $featureOptionsOff = [];
            $this->edit->model->caseFeatureChunks()->with('featureSet')->get()->pluck('featureSet.name', 'id')->each(function ($v, $k) use (&$featureOptions, &$featureOptionsOff) {
                $featureOptions[$k . ':' . CaseFeatureChunk::STATUS_ACTIVE] = $v;
                $featureOptionsOff[] = $k . ':' . CaseFeatureChunk::STATUS_INACTIVE;
            });
            $this->edit->add('caseFeatureChunks.status', 'Feature Sets', 'checkboxgroup')->options($featureOptions)->addHidden($featureOptionsOff);
        }

        if ($user->hasPermissionTo('pick district')) {
            $this->edit->add('district_id', 'District', 'select')->options(\App\Models\Features\District::pluck('name', 'id')->all());
        } else {
            // TODO: check this is enforced by Rapyd correctly on return to serverside
            $this->edit->add('district_id', 'District', 'hidden')->insertValue($user->affiliation->district_id)->mode('readonly');
        }

        $this->edit->add('centerLatLng', 'Centre', 'osmap')->insertValue(['lat' => '55', 'lon' => '-6'])->attributeIsLatLon(true);

        $this->edit->add('begins', 'Beginning Time', 'datetime')->insertValue(\Carbon\Carbon::now());
        $this->edit->add('ends', 'Ending Time', 'datetime')->insertValue(\Carbon\Carbon::now()->addHours(10));

        $this->edit->saved(function () use ($entity) {
            if ($this->request->get('step') == '1') {
                return redirect(url('panel', $entity . '/edit') . '?step=2&modify=' . $this->edit->model->id);
            } else {
                return redirect(url('panel', $entity . '/all'));
            }
        });

        return $this->returnEditView('panelViews::edit', ['title' => 'Edit Target Zone']);
    }

    public function downloadReport(OrpReportHelper $helper, $id)
    {
        $caseContext = CaseContext::findOrFail($id);
        $report = $helper->exportToTemporaryFile($caseContext);
        return Response::download(
            $report,
            'ourragingplanet-report.html',
            ['Content-Type' => 'text/html']
        )->deleteFileAfterSend(true);
    }
}
