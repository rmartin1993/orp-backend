@startuml

' http://humaan.com/using-uuids-with-eloquent-in-laravel/

skinparam class {
  BackgroundColor<<SimulationTier>> Wheat
}

skinparam class {
  BackgroundColor<<CaseTier>> Pink
}

skinparam class {
  BackgroundColor<<AbstractTier>> Cyan
}

skinparam class {
  BackgroundColor<<Interaction>> Linen
}

skinparam class {
  BackgroundColor<<Users>> DarkSeaGreen
}

skinparam class {
  BackgroundColor<<Features>> Violet
}

together {
  class District <<Features>> {
    id
    slug
    name
  }
  class FeatureSet <<Features>> {
    id
    slug
    name
  }
}

together {
  class Phenomenon <<AbstractTier>> {
    id
    slug
    name
    symbol
    color
  }
  class Combination <<AbstractTier>> {
    id : uuid
    status
    owner : RegUser
  }
  class NumericalModel <<AbstractTier>> {
    uuid
    name
    definition
    owner : RegUser
  }
  class Analytic <<AbstractTier>> {
    id
    code
    name
  }
  class Resource <<AbstractTier>> {
    id
    code
    name
    description
  }
}
together {
  class Simulation <<SimulationTier>> {
    id : uuid
    settings
    definition
    status
    center : point
    extent : polygon
    begins : datetimetz
    ends : datetimetz
    owner : RegUser
    result : json
  }
  class FeatureArc <<SimulationTier>> {
    id
    arc : json
  }
  class ResultFeatureChunk <<SimulationTier>> {
    id
    time : datetimetz
    chunk : json
    arc : json
  }
  class ResultResource <<SimulationTier>> {
    id
    quantity
    duration
  }
  class ResultAnalytic <<SimulationTier>> {
    id
    value
  }
}
together {
  class CaseFeatureChunk <<CaseTier>> {
    id
    chunk : json
    status
  }
  class CaseContext <<CaseTier>> {
    id : uuid
    name
    center : point
    extent : polygon
    begins : datetimetz
    ends : datetimetz
    owner : RegUser
  }
}

together {
  class Affiliation <<Users>> {
    id
    slug
    name
  }
  class User <<Users>> {
    id : uuid
    identifier
  }
  class Notification <<Users>> {
    id : uuid
    text
  }
  class RegUser <<Users>> extends User {
    email
    forename
    surname
  }
  class MinUser <<Users>> extends User {
  }
}

together {
  class Challenge <<Interaction>> {
    id : uuid
    text
    subtext
    options
    correct
    mark
  }
  class ChallengeTemplate <<Interaction>> {
    id
    slug
    text
    subtext
    options
    correct
    mark
    global : boolean
  }
  class Worksheet <<Interaction>> {
    id : uuid
    progress
    worker : User
  }
  class WorksheetAnswer <<Interaction>> {
    id
    first
    final
    mark
  }
  class Informational <<Interaction>> {
    id : uuid
    text
    subtext
    time : datetime
    creator : User
  }
  class InformationalType <<Interaction>> {
    id
    slug
    name
  }
}

together {
  class CachedDataServerFeature {
    id
    feature_id
    location
    extent
    json
  }
  class CachedDataServerFeatureSet {
    id
    name
    owner
    license_title
    license_url
    uri
    data_server
    data_server_set_id
  }
}

CachedDataServerFeature "1" *-- "*" CachedDataServerFeatureSet

User "1" *-- "*" User : teaches

NumericalModel "1" - "*" Combination
Combination "1" - "*" Simulation

CaseContext "1" <-- "*" CaseFeatureChunk
CaseContext --> District

Affiliation "*" - "*" Combination
Affiliation "1" o-- "*" User
District "0..1" - "*" Affiliation

FeatureSet "1" <-- "*" CaseFeatureChunk
FeatureSet "1" <-- "*" ResultFeatureChunk
District "*" - "*" FeatureSet

Simulation "1" o-- "*" ResultFeatureChunk

Simulation "1" o-- "*" FeatureArc

Notification -> User

Phenomenon "1" <-- "*" NumericalModel

ChallengeTemplate "*" - "0..1" Analytic
Analytic "*" - "*" NumericalModel
Challenge --> ChallengeTemplate

Informational --> InformationalType

FeatureArc --> CaseFeatureChunk

ResultResource --> Resource
ResultResource --> Simulation
ResultAnalytic --> Simulation
ResultAnalytic "0..1" - "*" Challenge
ResultAnalytic --> Analytic

CaseContext "1" <-- "*" Simulation
CaseContext "1" <-- "*" Challenge
CaseContext "1" <-- "*" Informational
CaseContext "1" <-- "*" Worksheet
Worksheet "1" --- "*" WorksheetAnswer
WorksheetAnswer "*" --- "1" Challenge

@enduml
