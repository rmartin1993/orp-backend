#!/bin/bash

models=$(ls -1tr app/Models/*.php  | cut -f3 -d\/ | cut -f1 -d\.)

function scaffold {
    sudo docker-compose -f docker-compose-artisan.yml run artisan infyom:api_scaffold $1 --fromTable --tableName=$2
}

for model in $models
do
    table="$(echo $model | sed -e 's/\([A-Z]\)/_\L\1/g' -e 's/^_//')s"
    echo $model $table
    scaffold $model $table
done
