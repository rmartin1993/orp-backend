# orp-backend

> Back-end infrastructure for the OurRagingPlanet project

# OurRagingPlanet

## Our Chaotic World in Context

![Our Raging Planet Globe on NI Logo](/media/logo.png "Our Raging Planet" =250x)

### Introduction

How would the flash-floods of South America, the earthquakes of New Zealand, the wildfires of California or volcanic activity of the Pacific Rim affect Northern Ireland?

*This project teaches students the need for resilience and the implications of natural disasters through their own services and surroundings.*

It is a web application, with custom tools and interactive maps that allow students to apply basic simulations to their area in the region, using open data to estimate the impact on familiar local landmarks.

The app is mobile responsive, so it can be used smoothly, in its entirety, through classroom tablets, remotely as a homework assignment, or on desktop in ICT suites.  Where possible, graphic effects are applied to local video or photographs to illustrate the context.

Explanatory material linking students to the physical reasons such events do not happen in this region forms part of the workflow, including basic, accessible information tied to the syllabus content.

### In concrete terms?

For example, if an earthquake similar to those of 2010/11 in Canterbury, NZ had its epicentre below Carrickfergus or Limavady, the system would show names of heritage sites that may be permanently lost, local sports facilities rendered unusable through earthquake liquefaction, or suburbs likely to be worst affected. This aids students in understanding the human impact of such events, while making it clear that such events are constrained to the regions in which they occur.

### What's the Point?

Students gain an understanding of how the dynamic nature of the planet affects the human and natural world, in measurable terms to which they can easily relate.

They visually retain an understanding of natural disasters, the need for resilience and the impact on individuals in those contexts.

At higher levels, they will learn the physical basis and limitations of such events. Student task support and teacher monitoring of progress will be included, based on teacher input.

### How does it fit into the school setting?

In a classroom setting, students have set objectives to work through a range of scenarios contributing to age appropriate learning objects (discussed in further detail below).

The interface will be tailored to simplify use for both students and teachers, to enable quick set-up and minimize in-classroom preparation time.

As the resource will be accessible internally and externally, it may be included into both lesson plans and assignments.

The primary application of this resource is within the Northern Ireland Curriculum section "The World Around Us" at Key Stages 1 and 2 and several elements within the Geography syllabus at Key Stage 3.

The interface would be designed to be age appropriate primarily for Key Stage 2 and 3 students, with optional features that may be enabled for GCSE level.

### What's the Difference?

This approach is innovative in that it brings together several elements of the Earth Science syllabus that some students may find hard to relate to personally, with context-giving local data.

While online tools, such as the NI Rivers Agency flood map, demonstrate feasibility of producing a usable adverse condition web-based resource for NI,
this idea encompasses a number of environmental hazards, placing them in the same resource and workflow. Moreover, this resource will be tailored for classroom use from the ground up -
we have made provisions to include on-going professional teacher input and have incorporated feedback into our project outline based on secondary school teaching experience.

Where specific models are established, we use our own numerical simulation expertise to incorporate these - we are very keen for knowledgable individuals to contribute their own simulation modules!

Open data forms a critical part of the application, allowing users to better empathasize with the online simulation resource.
Moreover, this enables the entire data stream to be open and usable by schools without additional licensing costs and, while usable without further expansion or data,
can be enhanced as additional local information appears through OpenDataNI.

The software will be entirely open sourced, allowing schools not only to use it, but other developers around the world to submit material for inclusion, or adjust it
for their own use.

This project provides a simple API to be usable from student Python projects, so that they can download the tools and use them as software libraries in Raspberry Pi events
and extracurricular coding clubs.

This approach is also gains long-term sustainability through this innovative approach - by building a modular, extensible tool that students can learn to use programmatically,
enhancements can be cooperative and collaborative as future data becomes available, or new types of natural phenomena are incorporated.  However, the completed tool is able to
function completely without further development, as the required datasets are sufficient without further updating or future data release.

### What's the Open Data?

As an NI-funded project, we will incorporate a number of OpenDataNI datasets, in critical aspects of the application. Combined with the open source nature of the application,
additional open datasets may be incorporated into the application through developer extensions.

By providing the application as open source and supporting simple Python libraries, a pathway for interested teachers or students to using open data themselves is built from
classroom use to hobby or expansion use, enhancing both the educational and open attributes of the project, and raising the profile of open data applications.

Several datasets from organizations outside NI may be required, such as the Copernicus satellite data, which includes a range of useful data, both on terrain elevation and
on emergency mapping of a range of natural disasters. Data from the data.gov.uk portal relevant to Northern Ireland may also be used.

OpenDataNI datasets planned for inclusion:

 * Active Places NI - Sports Facilities Database
 * City Parks
 * Community Centres
 * Historic Parks and Gardens
 * Industrial Heritage Record
 * Library Locations in Northern Ireland
 * Lighting Assets
 * Listed Buildings Northern Ireland
 * Northern Ireland Railways Stations (and related datasets)
 * Playgrounds
 * Sports Pitches Playing Fields
 * Translink Bus Stop List
 * Translink Bus & Rail Stations

Datasets that may be also be used for derived data:

 * Northern Ireland Index of Production
 * Northern Ireland local authority collected municipal waste management
   statistics
 * Northern Ireland Water Bodies datasets
 * OSNI Open Data Townland Raster Maps (through data extraction)

### I'm not in NI!

For the moment, we are focusing on Northern Ireland - if you would be interested in funding or part-funding data analysis work for your local context (internally or through us), do get in touch.


# Buckram

## A Laravel Chart

Caveat: this structure is still in active development.

### Laravel with GitlabCI+Kubernetes extensions

For details of the upstream Laravel project, see https://github.com/laravel/laravel

Using the `.gitlab-ci.yml` file in this directory, the necessary steps are run to build, test and deploy to an
AWS Elastic Container Repository defined in your Gitlab Variables:

- AWS_ACCESS_KEY_ID
- AWS_DEFAULT_REGION
- AWS_ECR_URI
- AWS_SECRET_ACCESS_KEY

### Launching with Helm

You will require `kubectl`, a Kubernetes cluster configured and Docker installed (if building locally).

If using this source directory to build your images, make sure you run:
- `composer install`
before building. To build, run:
- `docker build -ti myimagename-nginx -f infrastructure/containers/nginx/Dockerfile .`
- `docker build -ti myimagename-phpfpm -f infrastructure/containers/phpfpm/Dockerfile .`
- `docker build -ti myimagename-artisan -f infrastructure/containers/artisan/Dockerfile .`
You will need these images to be available to your cluster from an accessible repository; the default demo
images built from this source are on Docker Hub. To use your own you will need to configure them in your `production.yaml` (see below).
In normal circumstances, your continuous delivery system will handle getting code to built images and on to
your cluster.

Start your Kubernetes cluster. If using minikube, then `minikube start`.

- If you have not used Helm on this cluster since it was created: `helm init`; if you have installed this Chart (Buckram Helm configuration) before, `helm list` to find the release name and `helm delete RELEASENAME`. Note that `helm init` will trigger a download of the Tiller pod (coordinates Helm actions on the cluster side), and until this has set itself up, Helm will not be ready for use.
- If you have not already, add an ingress controller: `helm install stable/traefik --namespace=kube-system --values traefik.yaml` (if you wish to have a Traefik dashboard, add `--set dashboard.enabled=true`)
- Copy `infrastructure/config/production.yaml.example` to `production.yaml` in a safe place and fill in the contents (e.g. APP_KEY)
- Make sure the hostname (where webpages should be served, not of the cluster) is correct in both places in `production.yaml`, and, if using minikube, that you have added it to your `/etc/hosts` file with the output of `minikube ip` as the address
- `helm dependencies update infrastructure/kubernetes/buckram`
- `helm install infrastructure/kubernetes/buckram --values PATH_TO_YOUR/production.yaml`

At present, the inter-dependencies are not ordered, so you may see, for instance, nginx starting before phpfpm and having to auto-restart before it gets the phpfpm server. Once `kubectl get pods` shows all pods running, your system should be good to go.

If using minikube, rather than a cloud provider, you will have to use a high numbered port to reach the ingress. Run `kubectl get svc --namespace=kube-system` and go to `HOSTNAME:PORT` in your browser, where `HOSTNAME` is the hostname used above, and `PORT` is the 3xxxx numbered port in the line with `traefik` in it (not `traefik-dashboard`)

To help web developers get familiar with Kubernetes, without trying to patch together multiple repositories,
the Helm chart for Kubernetes, container definitions and template secrets are under the `infrastructure`
subdirectory. However, bear in mind that, if you are experimenting
with the Kubernetes workflow by building and pushing images
it is recommended that you do so from a fresh clone of this repository so that
you do not include anything in the `.env` file (or elsewhere in the tree)
that should not end up in the test images. *In particular, make sure your `production.yaml`
is not in the tree*, especially if pushing experimentation images publicly.

(For those unfamiliar with Docker: in normal
workflow, the CI/CD train will clone your code repository
fresh and build the Docker images with your code inside,
then push to your chosen Docker image
repository, from where images may be deployed
to the cluster)

### Setting up

- `kubectl exec -ti PODNAMEPHPFPM -- php /var/www/app/artisan migrate`
- `kubectl exec -ti PODNAMEPHPFPM -- php /var/www/app/artisan db:seed --class=DatabaseSeeder`
To include sample data:
- `kubectl exec -ti PODNAMEPHPFPM -- php /var/www/app/artisan db:seed --class=SampleUsersTableSeeder`
- `kubectl exec -ti PODNAMEPHPFPM -- php /var/www/app/artisan db:seed --class=OpenDataSeeder`
- `kubectl exec -ti PODNAMEPHPFPM -- php /var/www/app/artisan db:seed --class=SimulationsTableSeeder`
