#!/bin/sh

# https://blog.redspread.com/using-awss-ec2-container-registry-with-k8s/

aws ecr get-login | sudo sh -
cat > tmp-aws-deploy-secret.yaml << EOF
apiVersion: v1
kind: Secret
metadata:
    name: aws-deploy-secret
data:
    .dockerconfigjson: $(sudo cat ~/.docker/config.json | base64 -w 0)
type: kubernetes.io/dockerconfigjson
EOF
