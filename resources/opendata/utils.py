# http://gis.stackexchange.com/questions/32418/python-script-to-convert-lat-long-to-itm-irish-transverse-mercator

from pyproj import Proj

class IrishGridConverter:
    def __init__(self):
        self._proj = Proj(init='epsg:29902')

    def __call__(self, x, y, inverse=True):
        lo, la = self._proj(x, y, inverse=inverse)
        return la, lo


def in_box(a, b, x1, y1, x2, y2):
    return (a > x1 and b > y1 and a < x2 and b < y2)
