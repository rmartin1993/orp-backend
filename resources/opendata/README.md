All files in this directory are licensed under the UK Open Government License unless otherwise stated.

BusStops                            owner: Translink
LightingAssets                      owner: TransportNI
IndustrialHeritageRecord            owner: Department of Communities
AreasOfArchaeologicalPotential      owner: Department of Communities
ActivePlacesNI                      owner: Sport NI
