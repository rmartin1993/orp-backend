@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Cached Data Server Feature Set
        </h1>
    </section>
    <div class="content">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row" style="padding-left: 20px">
                    @include('cached_data_server_feature_sets.show_fields')
                    <a href="{!! route('cachedDataServerFeatureSets.index') !!}" class="btn btn-default">Back</a>
                </div>
            </div>
        </div>
    </div>
@endsection
