<table class="table table-responsive" id="cachedDataServerFeatureSets-table">
    <thead>
        <th>Name</th>
        <th>Owner</th>
        <th>License Title</th>
        <th>License Url</th>
        <th>Uri</th>
        <th>Data Server</th>
        <th>Data Server Set Id</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($cachedDataServerFeatureSets as $cachedDataServerFeatureSet)
        <tr>
            <td>{!! $cachedDataServerFeatureSet->name !!}</td>
            <td>{!! $cachedDataServerFeatureSet->owner !!}</td>
            <td>{!! $cachedDataServerFeatureSet->license_title !!}</td>
            <td>{!! $cachedDataServerFeatureSet->license_url !!}</td>
            <td>{!! $cachedDataServerFeatureSet->uri !!}</td>
            <td>{!! $cachedDataServerFeatureSet->data_server !!}</td>
            <td>{!! $cachedDataServerFeatureSet->data_server_set_id !!}</td>
            <td>
                {!! Form::open(['route' => ['cachedDataServerFeatureSets.destroy', $cachedDataServerFeatureSet->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('cachedDataServerFeatureSets.show', [$cachedDataServerFeatureSet->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('cachedDataServerFeatureSets.edit', [$cachedDataServerFeatureSet->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>