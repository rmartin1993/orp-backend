<!-- Progress Field -->
<div class="form-group col-sm-6">
    {!! Form::label('progress', 'Progress:') !!}
    {!! Form::number('progress', null, ['class' => 'form-control']) !!}
</div>

<!-- Case Context Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('case_context_id', 'Case Context Id:') !!}
    {!! Form::text('case_context_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Worker Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('worker_id', 'Worker Id:') !!}
    {!! Form::text('worker_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('worksheets.index') !!}" class="btn btn-default">Cancel</a>
</div>
