@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Result Feature Chunk
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($resultFeatureChunk, ['route' => ['resultFeatureChunks.update', $resultFeatureChunk->id], 'method' => 'patch']) !!}

                        @include('result_feature_chunks.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection