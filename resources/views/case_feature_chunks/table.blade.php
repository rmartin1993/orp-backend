<table class="table table-responsive" id="caseFeatureChunks-table">
    <thead>
        <th>Chunk</th>
        <th>Case Context Id</th>
        <th>Feature Set Id</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($caseFeatureChunks as $caseFeatureChunk)
        <tr>
            <td>{!! $caseFeatureChunk->chunk !!}</td>
            <td>{!! $caseFeatureChunk->case_context_id !!}</td>
            <td>{!! $caseFeatureChunk->feature_set_id !!}</td>
            <td>
                {!! Form::open(['route' => ['caseFeatureChunks.destroy', $caseFeatureChunk->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('caseFeatureChunks.show', [$caseFeatureChunk->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('caseFeatureChunks.edit', [$caseFeatureChunk->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>