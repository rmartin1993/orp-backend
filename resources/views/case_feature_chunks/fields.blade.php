<!-- Chunk Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('chunk', 'Chunk:') !!}
    {!! Form::textarea('chunk', null, ['class' => 'form-control']) !!}
</div>

<!-- Case Context Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('case_context_id', 'Case Context Id:') !!}
    {!! Form::text('case_context_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Feature Set Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('feature_set_id', 'Feature Set Id:') !!}
    {!! Form::number('feature_set_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('caseFeatureChunks.index') !!}" class="btn btn-default">Cancel</a>
</div>
