@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Case Feature Chunk
        </h1>
    </section>
    <div class="content">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row" style="padding-left: 20px">
                    @include('case_feature_chunks.show_fields')
                    <a href="{!! route('caseFeatureChunks.index') !!}" class="btn btn-default">Back</a>
                </div>
            </div>
        </div>
    </div>
@endsection
