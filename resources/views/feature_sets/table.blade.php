<table class="table table-responsive" id="featureSets-table">
    <thead>
        <th>Slug</th>
        <th>Name</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($featureSets as $featureSet)
        <tr>
            <td>{!! $featureSet->slug !!}</td>
            <td>{!! $featureSet->name !!}</td>
            <td>
                {!! Form::open(['route' => ['featureSets.destroy', $featureSet->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('featureSets.show', [$featureSet->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('featureSets.edit', [$featureSet->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>