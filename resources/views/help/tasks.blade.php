@extends('help.template')
@section('content')

<div class='help-figure'>
    <img alt="Task in the Map Viewer" src='/images/screenshots/screenshot-task.png'/>
    <p>Figure: a <em>Task</em> being displayed in the <em>Map Viewer</em></p>
</div>

<h2>Managing Tasks</h2>

<p>Tasks can be added, edited and removed through your
Simulation's Task list. Open the edit dialog for your Simulation by clicking
the <em>Simulation</em> item in the left-hand navigation menu, and clicking its
name in the Simulation index. There is a link <em>Add or edit tasks</em> in the
Edit Simulation page, this will take you to the Simulation's Task list.</p>

<div class='help-figure'>
    <img alt="Listing all of your tasks" src='/images/screenshots/screenshot-as-tasks-all.png'/>
    <p>Figure: Listing your <em>Tasks</em></p>
</div>

<p>To add a new Task, click the <em>Add</em> button at the top-right. There are
(at least) two available types of Task: a plain text Task (<em>Set a Task</em>)
and a multichoice question. Select one and click <em>Save</em>. This will bring
you to a dialog that lets you configure your new task.</p>

<p>If you chose
<em>Set a Task</em>, you will be able to choose the <em>Title</em> that appears
in bold text at the top of the description in the <em>Tasks</em> popup within
the Map Viewer. You should also choose where on the Map Viewer's Timeline this
should appear for students, in terms of the number of seconds after the
Simulation start time. Finally, you can fill the text for the body of the
popup, under <em>Content</em>.</p>

<p>If you choose <em>Multichoice</em>, you
will be able to choose the <em>Title</em>, <em>Content</em> and <em>Seconds
after simulation start</em>, as above, as well as four possible multichoice
answers. You should also pick the correct answer: a, b, c or d.</p>

<div class='help-figure'>
    <img alt="Creating/editing a task" src='/images/screenshots/screenshot-as-multichoice.png'/>
    <p>Figure: Creating/editing a <em>Task</em></p>
</div>

@endsection
