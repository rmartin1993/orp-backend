@extends('help.template')
@section('content')

<h1>About OurRagingPlanet</h1>

<p>This is an educational resource intended to help students engage with the
human impact of natural disasters by placing global phenomena in a local
context. To do this, we bring together public data (drawing heavily on
<a href='https://www.opendatani.gov.uk/'>OpenDataNI</a>'s platform), open mapping tools, and basic simulation tools.</p>

<h2>Requirements</h2>

<p>OurRagingPlanet requires modern browsers, either on
desktops or tablets. Recent versions of Chrome or Firefox are recommended.</p>

<h2>Getting started</h2>

<p>OurRagingPlanet has two sides: a <a href='/help/students'>Map Viewer</a> for
teachers and students to explore simulated scenarios, and an <a href='/help/teachers'>Admin Panel</a> for
teachers to set up scenarios and tasks for students to complete.</p>

<p>To create
a new scenario for your students to explore, you will first need to identity a
<a href='/help/target-zones'>Target Zone</a>. This is a location and a time window for phenomena to
occur. Each of these can have one or more Simulations created for different
natural disasters. For instance, you may wish to add one Simulation for an
earthquake and another for a storm surge.</p>

<p>Once you have created a
<a href='/help/simulation'>Simulation</a>, you should go to the Simulation index, by choosing
Simulations in the left-hand navigation menu, and clicking the
<em>Display</em> link in the row of the Simulation you have created. This will
take you to the Map Viewer. Copy the link from your browser to give to your
students. They will see the same Map Viewer interface on following the link.
Note that anyone with this link can view your Simulation in the Map Viewer.</p>

<p><em>It is strongly recommended that you pick a limited set of features when creating your Target Zone
    if you are in a densely populated area. A large number of feature points will slow down the interface
    for your students.</em></p>

<div class='help-figure'>
    <img alt="Hurricane storm surge calculation shown in Map Viewer" src='/images/screenshots/screenshot-newsfeed.png'/>
    <p>Figure: Hurricane storm surge calculation shown in <em>Map Viewer</em></p>
</div>

<h1>Basics</h1>

<p>For teachers, there are several
concepts to be aware of before starting with OurRagingPlanet. These are:</p>

<ol>
    <li>
            <em><a href='/help/students'>Map Viewer</a></em>: this is the front-end used by students and
            teachers to explore natural disasters and their consequences visually. It
            may be found at <a href='https://maps.ourragingplanet.com'>https://maps.ourragingplanet.com</a>
    </li>
    <li>
            <em><a href='/help/teachers'>Admin Panel</a></em>: this is the back-end used by teachers to create and
            configure scenarios for students to explore. It may be found at
            <a href='https://maps.ourragingplanet.com'>https://maps.ourragingplanet.com/panel</a>
    </li>
    <li>
            <em><a href='/help/simulations'>Simulation</a></em>: this
            is an approximation of progressive damage or risk over time, which can be
            shown on a map. It shows the impact on local services and facilities, and may
            have attached tasks or information for students
    </li>
    <li>
            <em><a href='/help/target-zones'>Target Zone</a></em>: consists of a location, a beginning time and an end time. Multiple Simulations
            may be performed on a single Target Zone, perhaps for different types of
            natural disaster. The beginning time is the initial time shown on the Timeline
            when students open the Map Viewer to view your Simulations for this Target
            Zone. The end time is the cut-off point on the Timeline after which any
            Simulations for this Target Zone will show no change.
    </li>
    <li>
            <em><a href='/help/informationals'>Informational</a></em>: these appear in the <em>News Feed</em> on the
            bottom left-hand side of the Map Viewer. They represent social media,
            newsflashes, or generally useful information that teachers can add and attach
            to a specific point in time in a Simulation
    </li>
    <li>
            <em><a href='/help/tasks'>Task</a></em>: these
            appear in the <em>Tasks</em> on the bottom right-hand side of the Map Viewer.
            They may be tasks or multichoice questions set by teachers and attached to a
            point in time in a Simulation
    </li>
    <li>
            <em>Timeline</em>: This crosses the
            middle of the bottom of the Map Viewer, and allows students to explore an
            unfolding emergency by sliding the vertical bar back and forward. This can be
            switched between an hour-based and a day-based view using the <em>Hour</em> and
            <em>Day</em> toggle buttons on the right-hand side. Informationals and Tasks
            appear on the timeline as <em>News</em> and <em>Task</em> boxes, respectively.
            As you navigate back and forward in the <em>News Feed</em> and <em>Tasks</em>
            popups, the timeline will jump to the relevant Informational or Task.
    </li>
</ol>

<h1>Topics</h1>

<ul>
    <li><a href='/help/teachers'>Admin Panel</a></li>
    <li><a href='/help/students'>Map Viewer (student interface)</a></li>
    <li><a href='/help/lessons'>Sample Lessons</a></li>
    <li><a href='/help/simulation'>Simulations</a></li>
    <li><a href='/help/informationals'>Informationals</a></li>
    <li><a href='/help/target-zones'>Target Zones</a></li>
    <li><a href='/help/tasks'>Tasks</a></li>
</ul>

<h1>About Us</h1>

<p>OurRagingPlanet was implemented by a group of small
businesses in Northern Ireland, as part of an open source collaboration, led by
Flax &amp; Teal Limited. Many thanks are due for practical and financial
support to the OpenDataNI team, staff of CCEA, the Department of Economy and
the Department of Finance of Northern Ireland. This resource is not endorsed by
any public body, and has been independently designed and implemented.</p>

<p>If you have an interest in commissioning development of OurRagingPlanet for a
particular application or requirement, within or outside education, please get
in touch (info@ourragingplanet.com) - our design allows for a significant
degree of flexibility and extension.</p>

<hr/>

@endsection
