<table class="table table-responsive" id="numericalModels-table">
    <thead>
        <th>Name</th>
        <th>Definition</th>
        <th>Phenomenon Id</th>
        <th>Owner Id</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($numericalModels as $numericalModel)
        <tr>
            <td>{!! $numericalModel->name !!}</td>
            <td>{!! $numericalModel->definition !!}</td>
            <td>{!! $numericalModel->phenomenon_id !!}</td>
            <td>{!! $numericalModel->owner_id !!}</td>
            <td>
                {!! Form::open(['route' => ['numericalModels.destroy', $numericalModel->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('numericalModels.show', [$numericalModel->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('numericalModels.edit', [$numericalModel->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>