<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $challenge->id !!}</p>
</div>

<!-- Text Field -->
<div class="form-group">
    {!! Form::label('text', 'Text:') !!}
    <p>{!! $challenge->text !!}</p>
</div>

<!-- Subtext Field -->
<div class="form-group">
    {!! Form::label('subtext', 'Subtext:') !!}
    <p>{!! $challenge->subtext !!}</p>
</div>

<!-- Options Field -->
<div class="form-group">
    {!! Form::label('options', 'Options:') !!}
    <p>{!! $challenge->options !!}</p>
</div>

<!-- Correct Field -->
<div class="form-group">
    {!! Form::label('correct', 'Correct:') !!}
    <p>{!! $challenge->correct !!}</p>
</div>

<!-- Mark Field -->
<div class="form-group">
    {!! Form::label('mark', 'Mark:') !!}
    <p>{!! $challenge->mark !!}</p>
</div>

<!-- Challenge Template Id Field -->
<div class="form-group">
    {!! Form::label('challenge_template_id', 'Challenge Template Id:') !!}
    <p>{!! $challenge->challenge_template_id !!}</p>
</div>

<!-- Case Context Id Field -->
<div class="form-group">
    {!! Form::label('case_context_id', 'Case Context Id:') !!}
    <p>{!! $challenge->case_context_id !!}</p>
</div>

<!-- Result Analytic Id Field -->
<div class="form-group">
    {!! Form::label('result_analytic_id', 'Result Analytic Id:') !!}
    <p>{!! $challenge->result_analytic_id !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $challenge->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $challenge->updated_at !!}</p>
</div>

