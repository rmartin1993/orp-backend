@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Result Analytic
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($resultAnalytic, ['route' => ['resultAnalytics.update', $resultAnalytic->id], 'method' => 'patch']) !!}

                        @include('result_analytics.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection