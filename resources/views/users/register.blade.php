@extends('panelViews::master')
@section('bodyClass')
register
@stop
@section('body')
    <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    
                    <div class="login-panel panel panel-default">
                     
                        @include('flash::message')

                        <div class="panel-body">
                            <div class="logo-holder">
                                <img src="{{asset(Config::get('panel.logo'))}}" />
                            </div>

                            @if (count($errors))
                                <ul>
                                    @foreach($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            @endif

                            <h1>Non-Academic User Registration</h1>

                            {!! Form::open(array('route' => 'users.store-register')) !!}
                                <fieldset>
                                    <div class="form-group">
                                        <label for="affiliation_code">Join existing Affiliation with access code: </label>
                                        <input class="form-control" placeholder="Affiliation Code" name="affiliation_code" type="text" autofocus>
                                    </div>
                                    <div class="form-group">
                                        <label for="affiliation_name">or create Affiliation with name: </label>
                                        <input class="form-control" placeholder="Affiliation Name" name="affiliation_name" type="text" autofocus>
                                    </div>
                                    <hr/>
                                    <div class="form-group">
                                        <label for="forename">First Name:</label>
                                        <input class="form-control" placeholder="First Name" name="forename" type="text" autofocus>
                                    </div>
                                    <div class="form-group">
                                        <label for="surname">Last Name:</label>
                                        <input class="form-control" placeholder="Last Name" name="surname" type="text" autofocus>
                                    </div>
                                    <div class="form-group">
                                        <label for="email">Email:</label>
                                        <input class="form-control" placeholder="{{ \Lang::get('panel::fields.email') }}" name="email" type="text" autofocus>
                                    </div>
                                    <div class="form-group">
                                        <label for="password">Password:</label>
                                        <input class="form-control" placeholder="{{ \Lang::get('panel::fields.password') }}" name="password" type="password" value="">
                                    </div>
                                    <div class="form-group">
                                        <label for="password_confirmation">Confirm Password:</label>
                                        <input class="form-control" placeholder="{{ \Lang::get('panel::fields.password') }}" name="password_confirmation" type="password" value="">
                                    </div>
                                    <hr/>
                                    <div class="form-group">
                                        <label for="district_id">Data District: </label>
                                        <select class="form-control" placeholder="District ID" name="district_id" type="text" autofocus>
                                            @foreach ($districts as $district)
                                                <option value="{{ $district->id }}">{{ $district->name }}</option>
                                                <option disabled>Contact us to request unlisted districts</option>
                                            @endforeach
                                        </select>
                                        <p>Data districts will be progressively expanded, but if you cannot see your required district above, please contact us to explore expedition options.</p>
                                    </div>
                                    <hr/>
                                    <div class="form-group">
                                        <label for="agreement">I agree and have authority to agree to the <a href="{{ config('ourragingplanet.legal.terms-and-conditions-link') }}">Terms and Conditions</a> and <a href="{{ config('ourragingplanet.legal.privacy-policy-link')}}">Privacy Policy</a> on behalf of my affiliation:</label>
                                        <input class="form-control" placeholder="I agree to terms and conditions, and privacy policy" name="agreement" type="checkbox" value="1">
                                    </div>
                                    <!-- Change this to a button or input when using this as a form -->
                                    <input type="submit"  class="btn btn-lg btn-success btn-block" value="Register">
                                </fieldset>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
    </div>
@stop

