<table class="table table-responsive" id="users-table">
    <thead>
        <th>Identifier</th>
        <th>Password</th>
        <th>Affiliation Id</th>
        <th>Teacher Id</th>
        <th>Forename</th>
        <th>Surname</th>
        <th>Email</th>
        <th>Remember Token</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($users as $user)
        <tr>
            <td>{!! $user->identifier !!}</td>
            <td>{!! $user->password !!}</td>
            <td>{!! $user->affiliation_id !!}</td>
            <td>{!! $user->teacher_id !!}</td>
            <td>{!! $user->forename !!}</td>
            <td>{!! $user->surname !!}</td>
            <td>{!! $user->email !!}</td>
            <td>{!! $user->remember_token !!}</td>
            <td>
                {!! Form::open(['route' => ['users.destroy', $user->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('users.show', [$user->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('users.edit', [$user->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>