<table class="table table-responsive" id="challengeTemplates-table">
    <thead>
        <th>Slug</th>
        <th>Text</th>
        <th>Subtext</th>
        <th>Options</th>
        <th>Correct</th>
        <th>Mark</th>
        <th>Global</th>
        <th>Analytic Id</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($challengeTemplates as $challengeTemplate)
        <tr>
            <td>{!! $challengeTemplate->slug !!}</td>
            <td>{!! $challengeTemplate->text !!}</td>
            <td>{!! $challengeTemplate->subtext !!}</td>
            <td>{!! $challengeTemplate->options !!}</td>
            <td>{!! $challengeTemplate->correct !!}</td>
            <td>{!! $challengeTemplate->mark !!}</td>
            <td>{!! $challengeTemplate->global !!}</td>
            <td>{!! $challengeTemplate->analytic_id !!}</td>
            <td>
                {!! Form::open(['route' => ['challengeTemplates.destroy', $challengeTemplate->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('challengeTemplates.show', [$challengeTemplate->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('challengeTemplates.edit', [$challengeTemplate->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>