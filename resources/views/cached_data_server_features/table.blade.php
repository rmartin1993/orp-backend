<table class="table table-responsive" id="cachedDataServerFeatures-table">
    <thead>
        <th>Cached Data Server Feature Set Id</th>
        <th>Feature Id</th>
        <th>Location</th>
        <th>Extent</th>
        <th>Json</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($cachedDataServerFeatures as $cachedDataServerFeature)
        <tr>
            <td>{!! $cachedDataServerFeature->cached_data_server_feature_set_id !!}</td>
            <td>{!! $cachedDataServerFeature->feature_id !!}</td>
            <td>{!! $cachedDataServerFeature->location !!}</td>
            <td>{!! $cachedDataServerFeature->extent !!}</td>
            <td>{!! $cachedDataServerFeature->json !!}</td>
            <td>
                {!! Form::open(['route' => ['cachedDataServerFeatures.destroy', $cachedDataServerFeature->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('cachedDataServerFeatures.show', [$cachedDataServerFeature->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('cachedDataServerFeatures.edit', [$cachedDataServerFeature->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>