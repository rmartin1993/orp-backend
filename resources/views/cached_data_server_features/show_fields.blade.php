<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $cachedDataServerFeature->id !!}</p>
</div>

<!-- Cached Data Server Feature Set Id Field -->
<div class="form-group">
    {!! Form::label('cached_data_server_feature_set_id', 'Cached Data Server Feature Set Id:') !!}
    <p>{!! $cachedDataServerFeature->cached_data_server_feature_set_id !!}</p>
</div>

<!-- Feature Id Field -->
<div class="form-group">
    {!! Form::label('feature_id', 'Feature Id:') !!}
    <p>{!! $cachedDataServerFeature->feature_id !!}</p>
</div>

<!-- Location Field -->
<div class="form-group">
    {!! Form::label('location', 'Location:') !!}
    <p>{!! $cachedDataServerFeature->location !!}</p>
</div>

<!-- Extent Field -->
<div class="form-group">
    {!! Form::label('extent', 'Extent:') !!}
    <p>{!! $cachedDataServerFeature->extent !!}</p>
</div>

<!-- Json Field -->
<div class="form-group">
    {!! Form::label('json', 'Json:') !!}
    <p>{!! $cachedDataServerFeature->json !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $cachedDataServerFeature->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $cachedDataServerFeature->updated_at !!}</p>
</div>

