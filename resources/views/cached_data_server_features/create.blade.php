@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Cached Data Server Feature
        </h1>
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">

            <div class="box-body">
                <div class="row">
                    {!! Form::open(['route' => 'cachedDataServerFeatures.store']) !!}

                        @include('cached_data_server_features.fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
