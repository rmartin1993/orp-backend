<table class="table table-responsive" id="informationalTypes-table">
    <thead>
        <th>Slug</th>
        <th>Name</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($informationalTypes as $informationalType)
        <tr>
            <td>{!! $informationalType->slug !!}</td>
            <td>{!! $informationalType->name !!}</td>
            <td>
                {!! Form::open(['route' => ['informationalTypes.destroy', $informationalType->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('informationalTypes.show', [$informationalType->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('informationalTypes.edit', [$informationalType->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>