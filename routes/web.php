<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('client');
});


Auth::routes();

Route::get('/home', 'HomeController@index');
Route::get('/help', function () {
    return Response::view('help.index');
});
Route::get('/help/{page}', function ($page) {
    return Response::view('help.' . $page);
});


Route::get('/register', 'UserController@createRegister');
Route::post('/register', ['uses' => 'UserController@storeRegister', 'as' => 'users.store-register']);

Route::post('/affiliations/{affiliationId}/add-simulations-30', 'AffiliationController@updateAddSimulations10');

Route::get('/panel/CaseTier:CaseContext/downloadReport/{id}', 'CaseTier\CaseContextController@downloadReport');
