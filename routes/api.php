<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/user', function (Request $request) {
    return $request->user();
})->middleware('auth:api');


Route::group(['prefix' => 'v1'], function () {
    // Route::resource('analytics', 'AnalyticAPIController');
    Route::resource('case-contexts', 'CaseContextAPIController');
    Route::resource('case-feature-chunks', 'CaseFeatureChunkAPIController');
    // Route::resource('challenges', 'ChallengeAPIController');
    // Route::resource('challenge-templates', 'ChallengeTemplateAPIController');
    // Route::resource('combinations', 'CombinationAPIController');
    // Route::resource('districts', 'DistrictAPIController');
    // Route::resource('feature-arcs', 'FeatureArcAPIController');
    // Route::resource('feature-sets', 'FeatureSetAPIController');
    // Route::resource('informationals', 'InformationalAPIController');
    // Route::resource('informational-types', 'InformationalTypeAPIController');
    Route::resource('affiliations', 'AffiliationAPIController');
    // Route::resource('numerical-models', 'NumericalModelAPIController');
    Route::resource('phenomenons', 'PhenomenonAPIController');
    // Route::resource('resources', 'ResourceAPIController');
    // Route::resource('result-analytics', 'ResultAnalyticAPIController');
    // Route::resource('result-feature-chunks', 'ResultFeatureChunkAPIController');
    // Route::resource('result-resources', 'ResultResourceAPIController');
    // Route::resource('roles', 'RoleAPIController');

    Route::resource('simulations', 'SimulationAPIController');
    Route::get('simulations/{id}/feature-arcs/{featureArcId}', 'SimulationAPIController@featureArcShow');
    Route::get('simulations/{id}/features', 'SimulationAPIController@featuresIndex');
    Route::get('simulations/{id}/features/{featureChunkId}', 'SimulationAPIController@featuresShow');
    Route::get('simulations/{id}/informationals', 'SimulationAPIController@informationalIndex');
    Route::get('simulations/{id}/challenges', 'SimulationAPIController@challengeIndex');
    Route::post('simulations/{id}/run', 'SimulationAPIController@run');

    // Route::resource('users', 'UserAPIController');
    // Route::resource('worksheet-answers', 'WorksheetAnswerAPIController');
    // Route::resource('worksheets', 'WorksheetAPIController');
});
