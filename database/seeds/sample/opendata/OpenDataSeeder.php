<?php

use Illuminate\Database\Seeder;

class OpenDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         $this->call(TranslinkRailwayStationsSeeder::class);
         $this->call(AreasOfArchaeologicalPotentialSeeder::class);
         $this->call(IndustrialHeritageRecordSeeder::class);
         //$this->call(LightingAssetsSeeder::class);
         $this->call(ActivePlacesNISeeder::class);
         $this->call(BusStopsSeeder::class);
         $this->call(LightingAssetsSeeder::class);
         $this->call(NICommunityCentreSeeder::class);
         $this->call(NILandfillSiteSeeder::class);
         $this->call(NISchoolSeeder::class);
         $this->call(TranslinkRailwayStationsSeeder::class);
    }
}
