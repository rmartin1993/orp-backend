<?php

use Phaza\LaravelPostgis\Geometries\LineString;
use Phaza\LaravelPostgis\Geometries\Point;
use Phaza\LaravelPostgis\Geometries\Polygon;
use GeoJson\GeoJson;
use Illuminate\Database\Seeder;
use Cocur\Slugify\Slugify;
use App\Models\CachedDataServerFeature;
use App\Models\CachedDataServerFeatureSet;
use App\Models\Features\FeatureSet;
use App\Models\Features\District;

class BusStopsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $dataSource = CachedDataServerFeatureSet::whereName('Translink Bus Stop List')->first();

      if ($dataSource)
          $dataSource->cachedDataServerFeatures()->delete();
      else
          CachedDataServerFeatureSet::create([
            'name' => 'Translink Bus Stop List',
            'owner' => 'Translink',
            'license_title' => 'UK-OGL',
            'license_url' => 'http://www.nationalarchives.gov.uk/doc/open-government-licence/version/3/',
            'uri' => 'https://www.opendatani.gov.uk/dataset/translink-bus-stop-list',
            'data_server' => 'opendatani',
            'data_server_set_id' => 'translink-bus-stop-list'
          ]);

      $dataSource = CachedDataServerFeatureSet::whereName('Translink Bus Stop List')->first();

      $district = District::whereName('Northern Ireland 1')->first();
      $featureType = FeatureSet::whereSlug('bus-stop')->first();
      $featureType->districts()->sync([
          $district->id => [
              'data_server_set_id' => $dataSource->data_server_set_id,
              'data_server' => 'opendatani',
              'status' => 0
          ]
      ]);

      $translinkJson = json_decode(file_get_contents(base_path() . '/resources/opendata/bus-stop-list-february-2016.geojson.json'));

      $translinkBusStops = GeoJson::jsonUnserialize($translinkJson);
      foreach ($translinkBusStops as $busStop) {
          $coordinates = $busStop->getGeometry()->getCoordinates();

          $feature = new CachedDataServerFeature;
          $properties = $busStop->getProperties();
          $feature->feature_id = $properties['LocationID'];
          $feature->location = new Point($coordinates[1], $coordinates[0]);
          $feature->json = json_encode($busStop);

          $feature->cached_data_server_feature_set_id = $dataSource->id;

          $feature->save();
      }
    }
}
