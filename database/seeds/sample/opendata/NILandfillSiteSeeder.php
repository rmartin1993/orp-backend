<?php

use Phaza\LaravelPostgis\Geometries\LineString;
use Phaza\LaravelPostgis\Geometries\Point;
use Phaza\LaravelPostgis\Geometries\Polygon;
use GeoJson\GeoJson;
use Illuminate\Database\Seeder;
use Cocur\Slugify\Slugify;
use App\Models\CachedDataServerFeature;
use App\Models\CachedDataServerFeatureSet;
use App\Models\Features\FeatureSet;
use App\Models\Features\District;
use League\Csv\Reader;

class NILandfillSiteSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $dataSource = CachedDataServerFeatureSet::whereName('NI Landfill Site')->first();

        if ($dataSource) {
            $dataSource->cachedDataServerFeatures()->delete();
        } else {
            CachedDataServerFeatureSet::create([
              'name' => 'NI Landfill Site',
              'owner' => 'Department of Education (NI)',
              'license_title' => 'UK-OGL',
              'license_url' => 'http://www.nationalarchives.gov.uk/doc/open-government-licence/version/3/',
              'uri' => 'https://www.opendatani.gov.uk/dataset/community-centres',
              'data_server' => 'opendatani',
              'data_server_set_id' => '00000000-0000-0000-0000-000000000003'
            ]);
        }

        $dataSource = CachedDataServerFeatureSet::whereName('NI Landfill Site')->first();

        $district = District::whereName('Northern Ireland 1')->first();
        $feature_type = FeatureSet::whereName('Waste Site')->first();
        $feature_type->districts()->sync([
            $district->id => [
                'data_server_set_id' => $dataSource->data_server_set_id,
                'data_server' => 'opendatani',
                'status' => 0
            ]
        ]);

        $landfillsCsv = Reader::createFromPath(base_path() . '/resources/opendata/authorised-landfill-sites-ni-2015-latlng.csv');
        $headerRow = $landfillsCsv->fetchOne();
        $landfillsCsv->setOffset(1);
        $n = 0;
        $landfillsCsv->each(function ($row) use (&$n, $dataSource, $headerRow) {
            $n++;
            $feature = new CachedDataServerFeature;
            $feature->feature_id = $n;
            $feature->location = new Point($row[7], $row[8]);
            $feature->json = json_encode(array_combine($headerRow, $row));
            $feature->cached_data_server_feature_set_id = $dataSource->id;
            $feature->save();
            return true;
        });
    }
}
