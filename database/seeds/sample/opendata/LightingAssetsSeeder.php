<?php

use Phaza\LaravelPostgis\Geometries\LineString;
use Phaza\LaravelPostgis\Geometries\Point;
use Phaza\LaravelPostgis\Geometries\Polygon;
use GeoJson\GeoJson;
use Illuminate\Database\Seeder;
use Cocur\Slugify\Slugify;
use App\Models\CachedDataServerFeature;
use App\Models\CachedDataServerFeatureSet;
use App\Models\Features\FeatureSet;
use App\Models\Features\District;


class LightingAssetsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data_source = CachedDataServerFeatureSet::whereName('TransportNI Lighting Assets')->first();

        if ($data_source) {
            //$data_source->cachedDataServerFeatures()->delete();
        } else {
            CachedDataServerFeatureSet::create([
                'name' => 'TransportNI Lighting Assets',
                'owner' => 'TransportNI',
                'license_title' => 'UK-OGL',
                'license_url' => 'http://www.nationalarchives.gov.uk/doc/open-government-licence/version/3/',
                'uri' => 'https://www.opendatani.gov.uk/dataset/lighting-assets',
                'data_server' => 'opendatani',
                'data_server_set_id' => '9615b5b6-3f11-4968-b337-f1dc1e9db829'
            ]);

            $data_source = CachedDataServerFeatureSet::whereName('TransportNI Lighting Assets')->first();

            $district = District::whereName('Northern Ireland 1')->first();
            $feature_type = FeatureSet::whereName('Street Light')->first();
            $feature_type->districts()->sync([
                $district->id => [
                    'data_server_set_id' => $data_source->data_server_set_id,
                    'status' => 0
                ]
            ]);


            $lightingAssets = ['lightingpointssouthern.geojson', 'lightingpointsnorthern.geojson', 'lightingpointseastern.geojson', 'lightingpointswestern.geojson'];
            foreach ($lightingAssets as $lightingAsset) {
                $lighting_json = json_decode(file_get_contents(base_path() . '/resources/opendata/' . $lightingAsset));

                $assets = GeoJson::jsonUnserialize($lighting_json);
                $slugify = new Slugify();
                $counter = 0;
                $counter_total = count($assets);
                foreach ($assets as $asset) {
                    $coordinates = $asset->getGeometry()->getCoordinates();

                    $feature = new CachedDataServerFeature;

                    $properties = $asset->getProperties();

                    $feature->feature_id = $properties['ITEM_IDENTITY_CODE'];
                    $feature->location = new Point($coordinates[1], $coordinates[0]);
                    $feature->json = json_encode($asset->getProperties());
                    $feature->cached_data_server_feature_set_id = $data_source->id;

                    $feature->save();

                    $counter++;
                    if ($counter % 1000 == 0) {
                        $this->command->info("{$lightingAsset}: Added {$counter}/{$counter_total} lighting assets");
                    }
                }
            }
        }
    }
}
