<?php

use App\Models\Users\User;
use App\Models\Users\Affiliation;
use App\Models\Features\District;
use App\Models\AbstractTier\Phenomenon;
use Illuminate\Database\Seeder;

class SampleUsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $affiliation = Affiliation::firstOrCreate(
            [
                'name' => 'Example School'
            ]
        );
        $affiliation->district_id = District::first()->id;
        $affiliation->save();

        $teacher = User::firstOrCreate(
            [
                'identifier' => 'teacher101',
                'forename' => 'Test',
                'surname' => 'Teacher',
                'password' => bcrypt('secret'),
                'affiliation_id' => $affiliation->id,
                'email' => 'testteach@example.com',
            ]
        );

        $teacher->assignRole('teacher');

        $stormSurge = Phenomenon::whereName('Hurricane Storm Surge')->first();
        $combination = $stormSurge->numericalModels()->first()->combinations()->first();
        $affiliation->combinations()->attach($combination);
    }
}
