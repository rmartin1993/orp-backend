<?php

use Phaza\LaravelPostgis\Geometries\LineString;
use Phaza\LaravelPostgis\Geometries\Point;
use Phaza\LaravelPostgis\Geometries\Polygon;
use Illuminate\Database\Seeder;
use App\Models\Features\FeatureSet;
use App\Models\Features\District;

class FeatureSetsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //DB::table('district_feature_set')->delete();

        //DB::table('districts')->delete();
        District::firstOrCreate([
            'name' => 'Northern Ireland 1',
            'slug' => 'northern-ireland-1'
        ]);

        //FeatureSet::delete();

        FeatureSet::firstOrCreate([
          'name' => 'My School',
          'slug' => 'my-school',
          'description' => 'Where we learn!'
        ]);

        FeatureSet::firstOrCreate([
          'name' => 'Focal Point',
          'slug' => 'focal-point',
          'description' => 'Focus point of the emergency'
        ]);

        FeatureSet::firstOrCreate([
          'name' => 'School Locations',
          'slug' => 'school-locations',
          'description' => 'Where we learn!'
        ]);

        FeatureSet::firstOrCreate([
          'name' => 'School Locations',
          'slug' => 'school-locations',
          'description' => 'Where we learn!'
        ]);

        FeatureSet::firstOrCreate([
          'name' => 'Bus Stop',
          'slug' => 'bus-stop',
          'description' => 'Pick up and drop off point for buses'
        ]);

        FeatureSet::firstOrCreate([
          'name' => 'Street Light',
          'slug' => 'street-light',
          'description' => 'Lighting for street users'
        ]);

        FeatureSet::firstOrCreate([
          'name' => 'Unexplored Cultural Heritage',
          'slug' => 'unexplored-cultural-heritage',
          'description' => 'Mysteries of our past waiting to be discovered'
        ]);

        FeatureSet::firstOrCreate([
          'name' => 'Industrial Heritage',
          'slug' => 'industrial-heritage',
          'description' => 'Artifact of our industrial past'
        ]);

        FeatureSet::firstOrCreate([
          'name' => 'Railway Station',
          'slug' => 'railway-station',
          'description' => 'Stopping points for local trains'
        ]);

        FeatureSet::firstOrCreate([
          'name' => 'Sports Facility',
          'slug' => 'sports-facility',
          'description' => 'Venue for sports or exercise'
        ]);

        FeatureSet::firstOrCreate([
          'name' => 'Community Centre',
          'slug' => 'community-centre',
          'description' => 'Venue for community activities'
        ]);

        FeatureSet::firstOrCreate([
          'name' => 'Waste Site',
          'slug' => 'waste-site',
          'description' => 'Location where waste may be processed or stored'
        ]);

        FeatureSet::firstOrCreate([
          'name' => '90% Risk',
          'slug' => 'result-90-risk',
          'description' => 'Area within which risk of damage is at a nominal 90% level'
        ]);

        FeatureSet::firstOrCreate([
          'name' => '70% Risk',
          'slug' => 'result-70-risk',
          'description' => 'Area within which risk of damage is at a nominal 70% level'
        ]);
    }
}
