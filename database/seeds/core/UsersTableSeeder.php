<?php

use App\Models\Users\User;
use App\Models\Users\Role;
use App\Models\Users\Affiliation;
use App\Models\Features\District;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $affiliation = Affiliation::firstOrCreate(
            [
                'name' => 'Super Administrators'
            ]
        );

        $admin = User::firstOrCreate(
            [
                'identifier' => 'administrator',
                'forename' => 'Root',
                'surname' => 'Administrator',
                'email' => 'admin@example.com',
                'password' => bcrypt('change-this'),
                'affiliation_id' => $affiliation->id
            ]
        );
        $admin->assignRole('admin');
    }
}
