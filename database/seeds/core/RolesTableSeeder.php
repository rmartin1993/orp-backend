<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('permissions')->delete();

        $admin = Role::firstOrCreate(['name' => 'admin']);
        $teacher = Role::firstOrCreate(['name' => 'teacher']);

        $permissions = [
            Permission::create(['name' => 'access_backend']),
            Permission::create(['name' => 'pick district']),
            Permission::create(['name' => 'view /Role/all']),
            Permission::create(['name' => 'view /Role/edit']),
            Permission::create(['name' => 'view /Permission/all']),
            Permission::create(['name' => 'view /Permission/edit']),
            Permission::create(['name' => 'view /Link/all']),
            Permission::create(['name' => 'view /Link/edit']),
        ];

        foreach ($permissions as $permission) {
            $admin->givePermissionTo($permission);
        }

        $teacher->givePermissionTo('access_backend');

        $adminAndTeacher = [
            Permission::create(['name' => 'view /SimulationTier:Simulation/all']),
            Permission::create(['name' => 'view /SimulationTier:Simulation/edit']),
            Permission::create(['name' => 'view /Interaction:Challenge/all']),
            Permission::create(['name' => 'view /Interaction:Challenge/edit']),
            Permission::create(['name' => 'view /Interaction:Informational/all']),
            Permission::create(['name' => 'view /Interaction:Informational/edit']),
            Permission::create(['name' => 'view /CaseTier:CaseContext/all']),
            Permission::create(['name' => 'view /CaseTier:CaseContext/edit']),
        ];

        foreach ([$admin, $teacher] as $role) {
            foreach ($adminAndTeacher as $permission) {
                $role->givePermissionTo($permission);
            }
        }
    }
}
