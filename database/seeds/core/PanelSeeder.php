<?php

use App\Models\Users\User;
use App\Models\Users\Role;
use App\Models\Users\Affiliation;
use App\Models\Features\District;
use Illuminate\Database\Seeder;

class PanelSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('links')->delete();

        // show Roles on panel
        DB::table('links')->insert(
            [
                'display' => 'Links',
                'url' => 'Link',
                'main' => '1',
                'show_menu' => '1'
            ]
        );

        // show Roles on panel
        DB::table('links')->insert(
            [
                'display' => 'Roles',
                'url' => 'Role',
                'main' => '1',
                'show_menu' => '1'
            ]
        );

        // show Permission on panel
        DB::table('links')->insert(
            [
                'display' => 'Permissions',
                'url' => 'Permission',
                'main' => '1',
                'show_menu' => '1'
            ]
        );

/* Serverfireteam\Panel\Admin points to App\User
        // show Admin on panel
        DB::table('links')->insert(
            [
                'display' => 'Admins',
                'url' => 'Admin', 
                'main' => '1',
                'show_menu' => '1'
            ]
        );
*/

        // show Case on panel
        DB::table('links')->insert(
            [
                'display' => 'Target Zones',
                'url' => 'CaseTier:CaseContext',
                'show_menu' => '1'
            ]
        );

        // show Simulation on panel
        DB::table('links')->insert(
            [
                'display' => 'Simulations',
                'url' => 'SimulationTier:Simulation',
                'show_menu' => '1'
            ]
        );

        // show Simulation on panel
        DB::table('links')->insert(
            [
                'display' => 'Informationals',
                'url' => 'Interaction:Informational',
                'show_menu' => '0'
            ]
        );

        // show Simulation on panel
        DB::table('links')->insert(
            [
                'display' => 'Challenges',
                'url' => 'Interaction:Challenge',
                'show_menu' => '0'
            ]
        );
    }
}
