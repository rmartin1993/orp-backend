<?php

use Illuminate\Database\Seeder;
use App\Models\AbstractTier\Combination;
use App\Models\AbstractTier\NumericalModel;
use App\Models\AbstractTier\Phenomenon;
use App\Models\Users\Affiliation;

class PhenomenonsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('phenomenons')->delete();

        $volcano = Phenomenon::create([
          'name' => 'Volcano',
          'symbol' => "/images/icon-volcano.svg",
          'color' => '#e50000'
        ]);
        $earthquake = Phenomenon::create([
          'name' => 'Earthquake',
          'symbol' => "/images/icon-earthquake.svg",
          'color' => '#c69a19'
        ]);
        Phenomenon::create([
          'name' => 'Flashflood',
          'symbol' => "/images/icon-flood.svg",
          'color' => '#0074e5'
        ]);
        $stormSurge = Phenomenon::create([
          'name' => 'Hurricane Storm Surge',
          'symbol' => "/images/icon-flood.svg",
          'color' => '#0074e5'
        ]);

        $superAdmin = Affiliation::whereName('Super Administrators')->first()->users()->whereForename('Root')->first();

        $stormSurgeModel = NumericalModel::create([
            'name' => 'based on Height',
            'definition' => json_encode([
                'type' => 'surge-elevation',
                'parameters' => [
                ]
            ]),
            'owner_id' => $superAdmin->id,
            'phenomenon_id' => $stormSurge->id
        ]);

        $stormSurgeCombination = Combination::create([
            'status' => Combination::STATUS_ACTIVE,
            'owner_id' => $superAdmin->id,
            'numerical_model_id' => $stormSurgeModel->id
        ]);

        $earthquakeModel = NumericalModel::create([
            'name' => 'based on Building Code',
            'definition' => json_encode([
                'type' => 'earthquake-shibata-study',
                'parameters' => [
                ]
            ]),
            'owner_id' => $superAdmin->id,
            'phenomenon_id' => $earthquake->id
        ]);

        $earthquakeSimpleModel = NumericalModel::create([
            'name' => 'using Danger Rings',
            'definition' => json_encode([
                'type' => 'concentric',
                'parameters' => [
                ]
            ]),
            'owner_id' => $superAdmin->id,
            'phenomenon_id' => $earthquake->id
        ]);

        $earthquakeCombination = Combination::create([
            'status' => Combination::STATUS_ACTIVE,
            'owner_id' => $superAdmin->id,
            'numerical_model_id' => $earthquakeModel->id
        ]);

        $earthquakeSimpleCombination = Combination::create([
            'status' => Combination::STATUS_ACTIVE,
            'owner_id' => $superAdmin->id,
            'numerical_model_id' => $earthquakeSimpleModel->id
        ]);

        $volcanoModel = NumericalModel::create([
            'name' => 'using Ash Danger Rings',
            'definition' => json_encode([
                'type' => 'volcano-ash-deposition',
                'parameters' => [
                ]
            ]),
            'owner_id' => $superAdmin->id,
            'phenomenon_id' => $volcano->id
        ]);

        $volcanoLavaModel = NumericalModel::create([
            'name' => 'using Lava Flow',
            'definition' => json_encode([
                'type' => 'volcano-lava',
                'parameters' => [
                ]
            ]),
            'owner_id' => $superAdmin->id,
            'phenomenon_id' => $volcano->id
        ]);

        $volcanoCombination = Combination::create([
            'status' => Combination::STATUS_ACTIVE,
            'owner_id' => $superAdmin->id,
            'numerical_model_id' => $volcanoModel->id
        ]);

        $volcanoLavaCombination = Combination::create([
            'status' => Combination::STATUS_ACTIVE,
            'owner_id' => $superAdmin->id,
            'numerical_model_id' => $volcanoLavaModel->id
        ]);
    }
}
