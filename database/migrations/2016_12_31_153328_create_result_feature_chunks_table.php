<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateResultFeatureChunksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('result_feature_chunks', function (Blueprint $table) {
            $table->increments('id');
            $table->json('chunk');
            $table->json('arc')->nullable();
            $table->bigInteger('time_offset')->nullable();

            $table->uuid('simulation_id');
            $table->foreign('simulation_id')->references('id')->on('simulations')->onDelete('cascade');

            $table->integer('feature_set_id')->nullable();
            $table->foreign('feature_set_id')->references('id')->on('feature_sets')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('result_feature_chunks');
    }
}
