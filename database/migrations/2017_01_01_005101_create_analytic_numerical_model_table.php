<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAnalyticNumericalModelTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('analytic_numerical_model', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('analytic_id')->unsignned();
            $table->foreign('analytic_id')->references('id')->on('analytics')->onDelete('cascade');

            $table->uuid('numerical_model_id');
            $table->foreign('numerical_model_id')->references('id')->on('numerical_models')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('analytic_numerical_model');
    }
}
